<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'manufacturer_id','product_id','user_id','quantity','totalprice'
    ];
}
