<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\OrderDetail;
use App\Customer;
use App\Mail\NewMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.page.home');
    }
    public function adminHome()
    {
        $orderArr = [];
        $orderByWeek = [];
        $orderByMonth = [];
        $currentTimeStr = strtotime(Carbon::now());
        $total = 0;
        $now = strtotime(Carbon::now()) - strtotime(Carbon::today());
        $orders = Order::all();
        foreach($orders as $order){
            $orderDateStr = strtotime($order->created_at);
            $timeMain = strtotime(Carbon::now()) - $orderDateStr;
            $startToWeekStr = strtotime(Carbon::now()->startOfWeek());
            $startToMonthStr = strtotime(Carbon::now()->startOfMonth());
            if($timeMain < $now){
                $orderArr[] = $order;
            }
            if($orderDateStr > $startToWeekStr && $orderDateStr < $currentTimeStr ){
                $orderByWeek[] = $order;
            }
            if($orderDateStr > $startToMonthStr && $orderDateStr < $currentTimeStr ){
                $orderByMonth[] = $order;
            }
        }
        foreach($orderArr as $orderItem){
            $total += $orderItem->total;
            $customerByDay = Customer::where('id',$orderItem->customer_id)->get();
            foreach($customerByDay as $customerDay){
                $orderItem['customerName'] = $customerDay->name;
            }
        }
        foreach($orderByWeek as $orderInWeek){
            $customerInWeek = Customer::where('id',$orderInWeek->customer_id)->get();
            foreach($customerInWeek as $customerWeek){
                $orderInWeek['customerName'] = $customerWeek->name;
            }
        }
        foreach($orderByMonth as $orderInMonth){
            $customerInMonth = Customer::where('id',$orderInMonth->customer_id)->get();
            foreach($customerInMonth as $customer){
                $orderInMonth['customerName'] = $customer->name;
            }
        }
        $newOrder = Order::latest()->get();
        foreach($newOrder as $orders) {
            $orders->customers = Customer::where('id',$orders->customer_id)->get();
        }
        $products = Product::orderBy('id', 'DESC')->limit(5)->get();
        foreach($products as $productbyCategory ){
            $productbyCategory->categoriesArr =  $productbyCategory->categories;
        }
        return view('admin.home',compact(['orderByMonth','orderByWeek','newOrder','orderArr','total','products']));
        return view('admin.home');
    }
    public function Shop()
    {
        return view ('frontend.page.shop');
    }
    public function aboutUs()
    {
        return view ('frontend.page.about');
    }
    public function detailOrder($id){
        $order = Order::find($id);
        return view ('admin.detail',compact('order'));
    }
    public function wareHouse(){
        return view ('admin.warehouse');
    }
    public function order(){
        $orders = Order::all();
        foreach($orders as $orderItem) {
            $orderItem->customer;
        }
        return view ('admin.orders.main',compact('orders'));
    }
    public function orderDetail($id){
        $order = Order::find($id);
        $order->customer->get();
        $order->orderdetails;
        foreach($order->orderdetails as $orderItem){
            $orderItem->products;
        }
        return view ('admin.orders.view',compact('order','orderItem'));
    }
    public function editOrder($id){
        $order = Order::find($id);
        return view ('admin.orders.edit',compact('order'));
    }
    public function updateOrder(Request $request, $id)
    {
        Order::where('id',$id)->update([
            'status'=> $request->status
        ]);
        return redirect('/order');
    }
}
