<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Category;
use App\Customer;
use App\Manufacturers;
use App\Product;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        foreach($products as $product){
            $product->categories;
        }
        return view ('admin.products.main',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturers::all();
        $category = Category::all();
        return view ('admin.products.create',compact('manufacturers','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'image' => 'required',
            'price' => 'required',
            'quantity' => 'required'
        ]);
        $data = [
            'name' => $request->name,
            'image'=> $request->image,
            'price'=> $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'parent_description' => $request->parent_description,
            'manufacturer_id' => $request->manufacturer_id
        ];
        if ($request->file('image')) {
            $result = $request->image->storeOnCloudinary('Storage');
            $data['image'] = $result->getPath();
        }
        $product = Product::create($data);
        $categories_id = $request->category_id;
        $product->categories()->attach($categories_id);
        return  redirect('/product')->with('message', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = Customer::all();
        $product = Product::find($id);
        // dd($product);
        return view ('admin.customers.main',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catebyProduct = [];
        $categories = Category::all();
        $manufacturers = Manufacturers::all();
        $product = Product::find($id);
        $cate = $product->categories()->get();
        $a = [];
        foreach($cate as $category){
            $catebyProduct[] = $category->id;
        }
        return view ('admin.products.edit',compact(['product','manufacturers','categories','catebyProduct']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::find($id);
        $data = [
            'name' => $request->name,
            'price'=> $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'parent_description' => $request->parent_description,
            'manufacturer_id' => $request->manufacturer_id
        ];

        if ($request->file('image')) {
            $result = $request->image->storeOnCloudinary('Storage');
            $data['image'] = $result->getPath();
        }

        $categories_id = $request->category_id;
        $products->categories()->sync($categories_id);
        $products->update($data);
        return redirect('/product');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders = Product::find($id);
        $orders->SoftDeletes();
        return redirect()->back();
    }
}
