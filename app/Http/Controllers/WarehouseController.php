<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturers;
use App\User;
use App\Invoice;
class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('admin.warehouse.main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $manufacturers = Manufacturers::all();
        $user = User::all();
        return view ('admin.warehouse.create',compact('products','manufacturers','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productID = json_decode($request->product_id,true);
        foreach($productID['id'] as $product)
        {
            $invoice = new Invoice();
            $invoice->product_id = $product;
            $invoice->manufacturer_id = $request->manufacturer_id;
            $invoice->user_id = $request->user_id;
        }
        $invoice->save();
        return redirect()->back();
    }
    public function renderProduct(Request $request)
    {
        $products = Product::find($request->data);
        $products->image = str_replace('public','',$products->image);
        return $this->jsonDataResult($products,200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function jsonMsgResult($errors, $success, $statusCode)
    {
        $result = [
            'errors' => $errors,
            'success' => $success,
            'statusCode' => $statusCode,
        ];

        return response()->json($result, $result['statusCode']);
    }

    /**
     * Return data json format
     */
    private function jsonDataResult($data, $statusCode)
    {
        $result = [
            'data' => $data,
            'statusCode' => $statusCode,
        ];
        return response()->json($result, $result['statusCode']);

    }
}
