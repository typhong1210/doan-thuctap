<?php

namespace App\Http\Controllers;

use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function showUploadForm()
    {
        return view('upload');
    }
    public function storeUploads(Request $request)
    {
        $result = $request->file->storeOnCloudinary('Storage');

        dd($result);

        return back()->with('success', 'File uploaded successfully');
    }
}
