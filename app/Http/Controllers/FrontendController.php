<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Manufacturers;
use App\Order;
use App\OrderDetail;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use App\Traits\showCategoryTrait;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
class FrontendController extends Controller
{
    const statusNull = -1;
    use showCategoryTrait;
    protected $product;
    protected $orderDetail;
    protected $customer;
    protected $order;

    public function __construct(Category $category,Product $product,OrderDetail $orderDetail,Customer $customer, Order $order)
    {
        $this->category  = $category;
        $this->product = $product;
        $this->orderDetail = $orderDetail;
        $this->customer = $customer;
        $this->order = $order;

    }
    public function listData()
    {
        $manufacturers = Manufacturers::all();
        $customers = $this->customer::all();
        $newProducts = Product::orderBy('id', 'DESC')->get();
        return view('frontend.page.home', compact(['newProducts','manufacturers', 'customers',
        ]));
    }
    public function productTab(Request $request)
    {
        if ($request->data == '*') {
            $products = $this->product::limit(12)->get();
        } else {
            $products = Category::find($request->data)->products()->limit(12)->get();
        }
        foreach ($products as $product) {
            // $product->image = str_replace('public', '', 'storage/' . $product->image);
            $product->price = number_format((float)$product->price, 0, '.', ',');
        }
        return $this->jsonDataResult($products, 200);
    }
    public function search()
    {
        return view('frontend.page.search');
    }
    public function searchByName(Request $request)
    {
        $data = [];
        $research = $this->product::where('name', 'like', '%' . $request->search . '%')->paginate(8);
        // dd(count($research));
        $data = [
            'search' => $research,
            'message' => '"' . count($research) . '" tìm thấy kết quả cho từ khoá"' . $request->search . '"'
        ];
        return view('frontend.page.search', compact('data'));
    }
    public function productDetail($id)
    {
        $product = $this->product::find($id);
        $manufacturer = Manufacturers::where('id',$product->manufacturer_id)->get();
        $latestProduct = Product::orderBy('id', 'DESC')->limit(5)->get();

        $countProduct = DB::table('order_detail')->selectRaw('product_id, count(product_id) as topOrder')->groupBy('product_id')->orderBy('topOrder','DESC')->limit(12)->get();
        $productIdCountArr = [];
        if(count($countProduct) > 0){
            foreach($countProduct as $count){
                $productIdCountArr[] = $count->product_id;
            }
            $topProduct = $this->product::whereIn('id',$productIdCountArr)->get();
        }else{
            $topProduct = $this->product::whereIn('id',$productIdCountArr)->get();
        }
        count($topProduct) > 0 ? $topProduct : [];
        return view('frontend.page.productdetail', compact('product','latestProduct','manufacturer','topProduct'));
    }
    public function addToCart(Request $request)
    {
        try {
            $total = 0;
            $product = $this->product::findOrFail($request->dataID);

            if (\Cart::count() == 0) {
                \Cart::add([
                    'id' => $request->dataID,
                    'name' => $product->name,
                    'qty' => $request->qty ? $request->qty : 1,
                    'price' => $product->price,
                    'weight' => 0,
                    'options' => [$product->image]
                ]);
                foreach (\Cart::content() as $cartItem) {
                    $total += $cartItem->qty * $cartItem->price;
                }
                return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
                ], 200);
            }
            $productByID = \Cart::content()->where('id', $request->dataID);

            if (count($productByID) > 0) {
                $total = 0;
                foreach ($productByID as $productByIdItem) {
                    $rowId = $productByIdItem->rowId;
                }
                if ($request->qty) {
                    $productByID[$rowId]->qty += $request->qty;
                } else {
                    $productByID[$rowId]->qty++;
                }
                foreach (\Cart::content() as $cartItem) {
                    $total += $cartItem->qty * $cartItem->price;
                }
                return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
                ], 200);
            }
            \Cart::add([
                'id' => $request->dataID,
                'name' => $product->name,
                'qty' => $request->qty ? $request->qty : 1,
                'price' => $product->price,
                'weight' => 0,
                'options' => [$product->image]
            ]);
            $total = 0;
            foreach (\Cart::content() as $cartItem) {
                $total += $cartItem->qty * $cartItem->price;
            }
            return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
            ], 200);
        } catch (\Exception $e) {
            return $this->jsonMsgResult($e->getMessage(),false,500);
        }
    }
    public function updateCart(Request $request)
    {
        $request->validate([
            'qty' => 'required|integer|min:0',
        ]);
        $total = 0;
        if ($request->dataID) {
            \Cart::content()[$request->dataID]->qty = $request->qty;
        }
        $subPrice = number_format(\Cart::content()[$request->dataID]->qty * \Cart::content()[$request->dataID]->price,0,'.',',').'đ';
        foreach (\Cart::content() as $cartItem) {
            $total += $cartItem->qty * $cartItem->price;
        }

        return $this->jsonDataResult([
            'subPrice' => $subPrice,
            'cart' => \Cart::content()[$request->dataID],
            'total' => number_format($total, 0, '.', ',') . 'đ'
        ],200);
    }
    public function Cart()
    {
        return view('frontend.page.cart');
    }
    public function removeCart(Request $request)
    {
        $total = 0;
        \Cart::remove($request->dataID);
        foreach(\Cart::content() as $cartItem){
            $total = $cartItem->qty * $cartItem->price;
        }
        return $this->jsonDataResult([
            'total' => $total,
            'count' => \Cart::count()
        ],200);
    }
    public function Checkout()
    {
        $total = 0;
        foreach (\Cart::content() as $cartItem) {
            $total += $cartItem->qty * $cartItem->price;
        }
        return view('frontend.page.checkout', compact('total'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function orderDetail(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'address' => 'required|string|max:255',
            'phone' => 'required',
            'mail' => 'required|email',
            'note' => 'required',
        ]);

        $newCustomer = new $this->customer($request->all());
        $newCustomer->save();
        $newOrder = new $this->order();
        $newOrder->customer_id = $newCustomer->id;
        $newOrder->ordernumber = 'abc';
        $newOrder->status = '1';
        $newOrder->code = str_split( Str::orderedUuid(),10)[2];
        $cart = \Cart::content();
        foreach($cart as $orderItem){
            $newOrder->total += $orderItem->qty * $orderItem->price;
        }
        $id = auth()->user()->id;
        $user = User::findOrFail($id);
        $newOrder->user_id = $user->id;
        $newOrder->save();
        $cartCollection = \Cart::content();
        foreach ($cartCollection as $product) {
            $orderDetail = new $this->orderDetail();
            $orderDetail->product_id = (int) $product->id;
            $orderDetail->order_id = $newOrder->id;
            $orderDetail->ordernumber = 1;
            $orderDetail->price = $product->price;
            $orderDetail->quantity = $product->qty;
            $orderDetail->total = $product->qty * $product->price;
            $orderDetail->save();
        }
        return redirect()->route('home.success', ['order_id' => $newOrder->id, 'customer_id' => $newCustomer->id]);
    }
    public function Success($order_id, $customer_id)
    {

        $dataSendMail = [];
        $orderDetail = $this->orderDetail::where('order_id', $order_id)->get();
        $totalPrice = 0;
        foreach ($orderDetail as $orderDetailByProduct) {
            $products = $this->product::where('id', $orderDetailByProduct->product_id)->get();
            $orderDetailByProduct->product = $products;
            $totalPrice += $orderDetailByProduct->quantity * $orderDetailByProduct->price;
        }
        $order = Order::where('customer_id', $customer_id)->get();
        foreach ($order as $orderItem) {
            $customerID = $orderItem->customer_id;
        }
        // \Cart::destroy();
        $customer = $this->customer::find($customerID);
        $dataSendMail['orderDetail'] = $orderDetail;
        $dataSendMail['customer'] = $customer;
        $dataSendMail['order'] = $order;

        Mail::to($dataSendMail['customer']->mail)->send(new \App\Mail\SendMail($dataSendMail));
        return view('frontend.page.successCheckout', compact('totalPrice', 'orderDetail', 'customer'));
    }
    public function showProduct(Request $request, $id)
    {
        $products = Category::find($id)->products()->paginate(8);
        return view('frontend.page.products', compact('products'));
    }
    public function showBrand($id)
    {
        $products = $this->product::where('manufacturer_id', $id)->paginate(8);
        $brands = Manufacturers::find($id);
        return view('frontend.page.brands', compact(['brands', 'products']));
    }
    private function jsonMsgResult($errors, $success, $statusCode)
    {
        $result = [
            'errors' => $errors,
            'success' => $success,
            'statusCode' => $statusCode,
        ];

        return response()->json($result, $result['statusCode']);
    }
    /**
     * Return data json format
     */
    private function jsonDataResult($data, $statusCode)
    {
        $result = [
            'data' => $data,
            'statusCode' => $statusCode,
        ];
        return response()->json($result, $result['statusCode']);

    }
    public function purchase()
    {

        $status = [1,2,3,4,5];
        $id = auth()->user()->id;
        // $user = User::findOrFail($id);
        $orderAll = $this->orderStatus(self::statusNull, $id);
        $orderConfirmtion = $this->orderStatus($status[0],$id);
        $orderConfirmed = $this->orderStatus($status[1],$id);
        $orderProcess = $this->orderStatus($status[2],$id);
        $orderDelivered = $this->orderStatus($status[3],$id);
        $orderCancel = $this->orderStatus($status[4],$id);
        return view('frontend.page.purchase',compact(
            ['orderAll','orderConfirmtion',
            'orderConfirmed','orderProcess',
            'orderDelivered','orderCancel'
            ]));
    }
    private function orderStatus(int $status,int $user_id){
        if($status < 0 ){
            $orders = Order::where('user_id',$user_id)->get();
        }else{
            $orders = Order::where('status',$status)->where('user_id',$user_id)->get();
        }
        foreach($orders as $order){
            $order->products;
            $order->orderdetails;
        }
        return $orders;
    }
}
