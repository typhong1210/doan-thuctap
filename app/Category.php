<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'id','name','parent_id'
    ];
    public function getchildcategory(){
        return $this->hasMany('App\Category','parent_id');
    }
    // public function products() {
    //     return $this->hasMany('App\Product','category_id');
    // }
    public function products(){
        return $this->belongsToMany('App\Product','product_category','category_id','product_id');
    }
}
