<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','name','description','price','image','quantity','manufacturer_id','checkbox','category_id','delete_at','parent_description'
    ];
    public function categorychild()
    {
        return $this->belongsTo('App\Categorychild','categorychildren_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
    public function manufactures() {
        return $this->belongsTo('App\Manufacturers','manufacturer_id');
    }

    public function categories(){
        return $this->belongsToMany('App\Category','product_category','product_id','category_id')->withPivot('category_id');
    }
    public function orderdetails(){
        return $this->belongsToMany('App\OrderDetail','product_id')->withPivot('product_id');
    }
    public function orders(){
        return $this->belongsToMany(Order::class, 'order_detail', 'product_id', 'order_id');
    }
}
