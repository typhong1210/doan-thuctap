<?php

namespace App\Providers;

use App\Category;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (env('APP_ENV') == 'Production') {
            URL::forceScheme('https');
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer('*', function ($view) {
            $category = Category::where('parent_id',0)->get();
            $view->with('menu',$category);
        });

        View::composer('*', function ($view) {
        $countProduct = DB::table('order_detail')->selectRaw('product_id, count(product_id)')->groupBy('product_id')->orderBy('product_id','DESC')->limit(12)->get();
        $productIdCountArr = [];
            if(count($countProduct) > 0){
                foreach($countProduct as $count){
                    $productIdCountArr[] = $count->product_id;
                }
                $topProduct = Product::whereIn('id',$productIdCountArr)->get();
            }else{
                $topProduct = Product::whereIn('id',$productIdCountArr)->get();
            }
            count($topProduct) > 0 ? $topProduct : [];
            $view->with('topProduct',$topProduct);
        });

        View::composer('*', function ($view) {
            $productRandom = Product::limit(12)->get();
            $view->with('productRandom',$productRandom);
        });
    }
}
