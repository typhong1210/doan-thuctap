<?php

namespace App\Traits;

trait showCategoryTrait {
    private $html = '';
    private $categoryIDArr = [];
    public function render($categories,$parentID = 0,$char = ''){
        foreach($categories as $category){
            // dd($category);
            if($category->parent_id == $parentID)
            {

                $this->html .= '<option value="'.$category->id.'">'.
                    $char.$category->name.
                 '</option>';
                $this->render($categories,$category->id,$char.'--- ');
            }
        }
        return $this->html;
    }
    public function getCategoryID($category,$parent_id){

        foreach($category as $categoryItem){
            if($categoryItem->id == $parent_id) {
                $this->categoryIDArr[] = $categoryItem->id;

                $this->getCategoryID($category,$categoryItem->parent_id);
            }

        }
        return $this->categoryIDArr;
    }
}

?>
