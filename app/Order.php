<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $dates = ['edited_at'];
    protected $fillable = [
        'id','customer_id','ordernumber','updated_at','created_at'
    ];
    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }
    public function orderdetails(){
        return $this->hasMany(OrderDetail::class,'order_id');
    }
    public function products(){
        return $this->belongsToMany(Product::class,'order_detail','order_id','product_id');
    }
    public function order(int $status,int $user_id)
    {
        $order = Order::where('status',$status)->where('user_id',$user_id)->get();
        return $order;
    }
}
