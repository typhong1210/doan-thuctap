<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_Product extends Model
{
    protected $fillable = [
        'invoice_id','product_id'
    ];
}
