<div class="top_bar">
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="tb_left pull-left">
                        <ul class="f-social" style="padding-top:0px;border-top:none;margin:0px;">
                            <li>
                                <a href="https://www.facebook.com" class="fa fa-facebook"></a>
                            </li>
                            <li>
                                <a href="https://www.twitter.com" class="fa fa-twitter"></a>
                            </li>
                            <li>
                                <a href="https://feedburner.google.com" class="fa fa-rss"></a>
                            </li>
                            <li>
                                <a href="mailto:email@website.com" class="fa fa-envelope"></a>
                            </li>
                            <li>
                                <a href="https://www.pinterest.com" class="fa fa-pinterest"></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com" class="fa fa-instagram"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="tb_center pull-left">
                        <ul>
                            <li>
                                <i class="fa fa-phone"></i> Hotline:
                                <a href="#">(+800) 2307 2509 8988</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <a href="#">Tổng đài tư vấn : 0772 442 422</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tb_right pull-right">
                        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                            <button style="display: none" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                    <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <!-- Authentication Links -->
                                        @guest
                                            <li class="nav-item">
                                                <a class="nav-link menu-login" href="{{ route('login') }}">{{ __('Đăng Nhập') }}</a>
                                            </li>
                                            @if (Route::has('register'))
                                                <li class="nav-item">
                                                    <a class="nav-link menu-login" href="{{ route('register') }}">{{ __('Đăng kí') }}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li class="nav-item dropdown">
                                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    {{ Auth::user()->name }}
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item purchase" href="{{ route('home.purchase') }}">Đơn Mua</a>
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        @endguest
                                    </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- HEADER -->
<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{asset('images/basic/logo.png')}}" class="img-responsive" alt="" /></a>
                </div>
                <div class="header-xtra pull-right">
                    <div class="topcart">
                        <span>
                            <i class="fa fa-shopping-cart"></i>
                        </span>
                        <div class="cart-info">
                            <small>You have
                                <em class="highlight">{{\Cart::count()}} item(s)</em> in your
                                shopping bag</small>
                                <?php $total = 0; ?>
                            @foreach(\Cart::content() as $cart)
                                <?php $total += ($cart->qty * $cart->price) ?>
                                <div class="ci-item ciItem-{{$cart->rowId}}">
                                    <img src="{{$cart->options[0]}}" width="80" alt="" />
                                    <div class="ci-item-info">
                                        <h5>
                                            <a href="./single-product.html">{{$cart->name}}</a>
                                        </h5>
                                        <p>{{$cart->qty}} x {{number_format($cart->price)}}đ</p>
                                        {{-- <div class="ci-edit">
                                            <a href="#" class="edit fa fa-edit"></a>
                                            <a href="{{route('home.remove',['id'=>$cart->rowId])}}" class="edit fa fa-trash"></a>
                                        </div> --}}
                                    </div>
                                </div>
                            @endforeach
                            <div class="ci-total">Thành tiền: {{number_format($total)}} đ</div>
                            <div class="cart-btn">
                                <a href="{{route('home.cart')}}">Giỏ hàng</a>
                                <a href="{{route('home.checkout')}}">Thanh Toán</a>
                            </div>

                        </div>
                    </div>
                    <div class="topsearch">
                        <span>
                            <i class="fa fa-search"></i>
                        </span>
                        <form method="POST" action="{{route('home.searchpost')}}" class="searchtop">
                            @csrf
                            <input type="text" name="search" placeholder="Search entire store here." />
                        </form>
                    </div>
                </div>
                <!-- Navmenu -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @foreach($menu as $category)
                            <li class="dropdown mmenu">
                                <a href="{{route('home.products',['id'=>$category->id])}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{$category->name}}</a>
                                <ul class="mega-menu dropdown-menu" role="menu">
                                    <li>
                                        <div>
                                            @foreach ($category->getchildcategory as $catechild)
                                                <a href="{{route('home.products',['id'=>$catechild->id])}}"><h5>{{$catechild->name}}</h5></a>
                                            @endforeach
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        @endforeach
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('home.shop')}}">Cửa hàng</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('home.about')}}">Giới thiệu</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
