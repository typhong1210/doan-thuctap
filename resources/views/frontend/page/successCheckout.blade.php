@extends('frontend.layout.app')
@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="box-success">
            <h1>BẠN ĐÃ ĐẶT ĐƠN HÀNG THÀNH CÔNG!</h1>
            <p>Chúng tôi sẽ giao đơn hàng đến bạn trong thời gian sớm nhất.<br> Dưới đây là chi tiết đơn hàng bạn vừa đặt.</p>
            <p>Vui lòng kiểm tra Email để xem thông tin đơn hàng.</p>
        </div>
    </div>
    <div class="col-lg-7 order-lg-first">
        <h2 class="h2-fiead">Địa chỉ nhận hàng</h2>
        <form>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 text-rights opsize"><b>Họ tên</b></label>
                <div class="col-sm-9">
                    <span class="opsize"> {{$customer->name}}</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 text-rights opsize"><b>Gmail</b></label>
                <div class="col-sm-9">
                    <span class="opsize"> {{$customer->mail}}</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 text-rights opsize"><b>Số điện thoại</b></label>
                <div class="col-sm-9">
                    <span class="opsize"> {{$customer->phone}}</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 text-rights opsize"><b>Địa chỉ</b></label>
                <div class="col-sm-9">
                    <span class="opsize"> {{$customer->address}}</span>
                </div>
            </div>
            <div class="form-group row ">
                <label for="inputPassword" class="col-sm-3 text-rights opsize"><b>Ghi chú</b></label>
                <div class="col-sm-9">
                    <span class="opsize"> {{$customer->note}}</span>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-5 info-cart">
        <h2 class="title-secres">Giỏ Hàng</h2>
        <ul class="list-unstyled">
            @foreach ($orderDetail as $order)
                @foreach ($order->product as $item)
                    <li>
                        <figure>
                            <a class="cart_list_product_img" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body">
                                <img src="{{$item->image}}" alt="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body" class="img-reponsive imds">
                            </a>
                        </figure>
                        <div class="list_content">
                            <h5>
                                <span class="name-ps">{{$item->name}}</span>
                            </h5>
                            <div class="quantity">
                                {{$order->quantity}} x
                                <span class="amount">
                                    <span class="money" style="color: #199427; font-size: 14px !important;">
                                        {{number_format($item->price)}} ₫
                                    </span>
                                </span>
                                <div class="pull-right tt">
                                    {{number_format($item->price * $order->quantity)}}₫
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            @endforeach
        </ul>

        <div class="load-lead">
            <div class="box-coupon">
                <p class="list-info-price" id="price-before" data-id="318000">
                    <small class="color-black">Tạm tính: </small> <span style="color:green">{{number_format($totalPrice)}}₫</span>
                </p>
            </div>
        </div>
        <div class="load-ship">
            <div class="box-coupon">
                <p class="list-info-price" id="priceship" data-id="0">
                    <small class="color-black">Phí vận chuyển: </small> <span>Miễn phí!</span>
                </p>
            </div>
        </div>
        <div class="subtotal">
            Thành tiền:
            <span>
                <span class="amount">
                    <strong class="money lucal" style="color: #199427;"> {{number_format($totalPrice)}}₫ </strong>
                </span>
            </span>
        </div>
        <div class="pttt">
            <span>* Phương thức thanh toán: Nhận hàng &amp; thanh toán tiền mặt tại nhà</span>
        </div>
    </div>

</div>
@endsection

