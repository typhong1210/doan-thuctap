@extends('frontend.layout.app')
@section('content')
<div class="container">
    <div class="store-section">
        <div class="store-item">
            <div class="row">
                <div class="col-lg-6">
                    <div id="shop-images"
                        class="swiper-container swiper-container-initialized swiper-container-horizontal">
                        <div class="swiper-wrapper photoswipe-gallery" data-pswp-uid="1"
                            style="transition-duration: 0ms; transform: translate3d(-540px, 0px, 0px);">
                            <figure class="swiper-slide thumb-item store-image swiper-slide-duplicate swiper-slide-prev"
                                itemprop="associatedMedia" itemscope="" data-swiper-slide-index="3"
                                style="width: 540px;"><a id="storeimage3" class="thumb-item-zoom"
                                    href="https://www.milem.vn/fileserver/images/file/fixsize-1080x720/upload/addon/shopimage/W5d6f83371d016/04.png?v=1.3"
                                    itemprop="contentUrl" data-size="1080x720"> <img width="540" height="360"
                                        class="swiper-lazy"
                                        data-srcset="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5d6f83371d016/04.png?v=1.3 1x, https://www.milem.vn/fileserver/images/file/fixsize-1080x720/upload/addon/shopimage/W5d6f83371d016/04.png?v=1.3 2x"
                                        data-src="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5d6f83371d016/04.png?v=1.3"
                                        src="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='540' height='360'><rect style='fill:rgb(245,245,245);' width='100%' height='100%' /></svg>">
                                    <div class="swiper-lazy-preloader"></div>
                                </a></figure>
                            <figure class="swiper-slide thumb-item store-image swiper-slide-active"
                                itemprop="associatedMedia" itemscope="" data-swiper-slide-index="0"
                                style="width: 540px;"><a id="storeimage0" class="thumb-item-zoom"
                                    href="https://www.milem.vn/fileserver/images/file/fixsize-1080x720/upload/addon/shopimage/W5c81f3979a971/milemstorefront.png?v=1.3"
                                    itemprop="contentUrl" data-size="1080x720">
                                    <picture>
                                        <source
                                            srcset="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5c81f3979a971/milemstorefront.png.webp?v=1.3"
                                            type="image/webp">
                                        <source
                                            srcset="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5c81f3979a971/milemstorefront.png?v=1.3"
                                            type="image/png"> <img width="540" height="360"
                                            class="swiper-lazy swiper-lazy-loaded"
                                            src="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5c81f3979a971/milemstorefront.png?v=1.3"
                                            srcset="https://www.milem.vn/fileserver/images/file/fixsize-540x360/upload/addon/shopimage/W5c81f3979a971/milemstorefront.png?v=1.3">
                                    </picture>
                                </a></figure>

                        </div>
                        <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span
                                class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
                                role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet"
                                tabindex="0" role="button" aria-label="Go to slide 2"></span><span
                                class="swiper-pagination-bullet" tabindex="0" role="button"
                                aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0"
                                role="button" aria-label="Go to slide 4"></span></div><span class="swiper-notification"
                            aria-live="assertive" aria-atomic="true"></span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="store-item-info">
                        <div class="store-name">Smile | Your Skincare & Perfumes Friend</div>
                        <div class="store-info-detail-body">
                            <ul class="list-unstyled">
                                <li class="store-info-line"><svg width="16" height="16" fill="#777"
                                        viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M1152 640q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm256 0q0 109-33 179l-364 774q-16 33-47.5 52t-67.5 19-67.5-19-46.5-52l-365-774q-33-70-33-179 0-212 150-362t362-150 362 150 150 362z">
                                        </path>
                                    </svg><span id="store-address" class="store-text-value">40 Nguyễn Huy Tưởng, Phường Hoà Minh, Quận Liên Chiểu, TP Đà Nẵng </span></li>
                                <li class="store-info-line"><svg width="16" height="16" fill="#777"
                                        viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M1600 1240q0 27-10 70.5t-21 68.5q-21 50-122 106-94 51-186 51-27 0-53-3.5t-57.5-12.5-47-14.5-55.5-20.5-49-18q-98-35-175-83-127-79-264-216t-216-264q-48-77-83-175-3-9-18-49t-20.5-55.5-14.5-47-12.5-57.5-3.5-53q0-92 51-186 56-101 106-122 25-11 68.5-21t70.5-10q14 0 21 3 18 6 53 76 11 19 30 54t35 63.5 31 53.5q3 4 17.5 25t21.5 35.5 7 28.5q0 20-28.5 50t-62 55-62 53-28.5 46q0 9 5 22.5t8.5 20.5 14 24 11.5 19q76 137 174 235t235 174q2 1 19 11.5t24 14 20.5 8.5 22.5 5q18 0 46-28.5t53-62 55-62 50-28.5q14 0 28.5 7t35.5 21.5 25 17.5q25 15 53.5 31t63.5 35 54 30q70 35 76 53 3 7 3 21z">
                                        </path>
                                    </svg><span id="store-phone" class="store-text-value">0898 238 258</span></li>
                                <li class="store-info-line d-flex"><svg width="16" height="16" fill="#777"
                                        viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M1024 544v448q0 14-9 23t-23 9h-320q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h224v-352q0-14 9-23t23-9h64q14 0 23 9t9 23zm416 352q0-148-73-273t-198-198-273-73-273 73-198 198-73 273 73 273 198 198 273 73 273-73 198-198 73-273zm224 0q0 209-103 385.5t-279.5 279.5-385.5 103-385.5-103-279.5-279.5-103-385.5 103-385.5 279.5-279.5 385.5-103 385.5 103 279.5 279.5 103 385.5z">
                                        </path>
                                    </svg>
                                    <p id="store-phone" class="store-text-value"> <span
                                            style="display: block; margin-bottom: 10px">Giờ hoạt động</span><span
                                            style="display: block; margin-bottom: 10px">Thứ 2 - Thứ 7: 08:00 -
                                            21:30</span><span style="display: block; margin-bottom: 10px">Chủ Nhật:
                                            09:30 - 20:30</span> </p>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
