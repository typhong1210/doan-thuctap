@extends('frontend.layout.app')
@section('content')
@push('css')

@endpush
    <div class="container" style="background: #fff;margin-top: 50px;">
        <div class="row">
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <div class="col-lg-9 col-md-9 col-sm-6">
                <div class="box-detail">
                    <div class="col-lg-6 col-md-6">
                        <div class="mar-bot-20">
                            <div class="box-img-detail">
                                <div class="config-alige">
                                    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails is-ready">
                                        <a href="{{ $product->image }}">
                                            <img src="{{ $product->image }}" class="img-reponsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-thumnail-detail">
                                <a href="javascript:void(0)" class="thumnail-pre btn-thumail">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </a>
                                <a href="javascript:void(0)" class="thumnail-nex btn-thumail">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="product-single">
                            <div class="ps-header">
                                <h3>{{ $product->name }}</h3>
                                <div class="ps-price"><span></span> {{ number_format($product->price) }}đ</div>
                                {!! $product->description !!}
                                @foreach ($manufacturer as $manufacturers)
                                    <div class="hanc">
                                        <p>Thương hiệu : {{ $manufacturers->name }}</p>
                                    </div>
                                @endforeach
                            </div>
                            <div class="space10"></div>
                            <div class="card-box">
                                <div class="hide  min-sch ">
                                    <span class="">: <span><b id="load-type">không có gì</b></span></span>
                                </div>
                                <div class="select-land hide ">
                                    <span class="">:</span>
                                    <ul class="ul-non ul-select">
                                    </ul>
                                    <div class="clr"></div>
                                </div>
                                <div class="inner-load">
                                    <div class="box-add">
                                        <div class="number-card">
                                            <span>Số lượng:</span>
                                            <input class="cart_quantity_input" type="number" name="quantity" value="1"
                                                autocomplete="off" size="2" min="1" max="10">
                                            <div class="clr"></div>
                                        </div>
                                        <div class="bst">
                                            <a href="javascript:;" data-addcart="{{ $product->id }}" class="addcart">
                                                <button type="button" class="btn-bts btn-atc">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm giỏ hàng
                                                </button>
                                            </a>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-products">
                    <div class="viewed">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9">
                                    <div class="bbb_viewed_title_container">
                                        <div class="bbb_viewed_title">
                                            <p>Sản phẩm bán chạy nhất</p>
                                        </div>
                                        <div class="bbb_viewed_nav_container">
                                            <div class="bbb_viewed_nav bbb_viewed_prev"><i class="fa fa-chevron-left"></i>
                                            </div>
                                            <div class="bbb_viewed_nav bbb_viewed_next"><i class="fa fa-chevron-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="bbb_viewed_slider_container">
                                        <div class="owl-carousel owl-theme bbb_viewed_slider">
                                            @foreach ($topProduct as $topproduct)
                                                <div class="owl-item">
                                                    <div
                                                        class="bbb_viewed_item discount ">
                                                        <div class="bbb_viewed_image"><img src="{{ $topproduct->image }}"
                                                                alt=""></div>
                                                        <div class="bbb_viewed_content text-center">
                                                            <div class="bbb_viewed_name"><a href="#">{{ $topproduct->name }}</a></div>
                                                            <div class="bbb_viewed_price">{{ number_format($topproduct->price) }}đ</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-description">
                    {!!$product->parent_description!!}
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top: 20px">
                <div class="box-left">
                    <div class="title-left">
                        <strong>DANH MỤC SẢN PHẨM</strong>
                    </div>
                    <div class="content-left">
                        <ul class="ul-non ul-left">
                            @foreach ($menu as $categories)
                                <li class=" ">
                                    <a href="{{ route('home.products', ['id' => $categories->id]) }}" title="Trang Điểm">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        {{ $categories->name }}
                                    </a>
                                    <ul class="ul-non ul-childs">
                                        @foreach ($categories->getchildcategory as $catechild)
                                            <li>
                                                <a href="{{ route('home.products', ['id' => $catechild->id]) }}"
                                                    title="Trang Điểm Mặt" class="">
                                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                                    {{ $catechild->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="box-left">
                    <div class="title-left">
                        <strong>SẢN PHẨM MỚI VỀ</strong>
                    </div>
                    <div class="content-left">
                        @foreach ($latestProduct as $itemProduct)
                            <div class="box-shas">
                                <div class="box-img">
                                    <a href="{{ route('home.detail', ['id' => $itemProduct->id]) }}"><img
                                            src="{{ $itemProduct->image }}" alt="{{ $itemProduct->name }}"></a>
                                </div>
                                <div class="box-content">
                                    <a href="{{ route('home.detail', ['id' => $itemProduct->id]) }}"
                                        title="{{ $itemProduct->name }}">
                                        {{ $itemProduct->name }}
                                    </a>
                                    <span>{{number_format($itemProduct->price)}}₫</span>
                                </div>
                                <div class="clr"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@push('scripts')
        <script>
            $('.addcart').on('click', function() {
                var data = $(this).data('addcart');
                var qty = $('input[name="quantity"]').val();
                $.ajax({
                    type: "POST",
                    url: "/addToCart",
                    data: {
                        dataID: data,
                        qty: qty
                    },
                    success: function(res) {
                        let html = `<small>You have
                                                                                        <em class="highlight">${res.data.count} item(s)</em> in your
                                                                                        shopping bag</small>`;
                        if (res.statusCode == 200) {
                            $.each(res.data.cart, function(i, v) {
                                html += `
                                                                                    <div class="ci-item">
                                                                                            <img src="${v.options[0]}" width="80" alt="" />
                                                                                            <div class="ci-item-info">
                                                                                                <h5>
                                                                                                    <a href="./single-product.html">${v.name}</a>
                                                                                                </h5>
                                                                                                <p>${v.qty} x ${v.price}đ</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    `
                            })
                            html += `
                                                                                    <div class="ci-total">Thành tiền: ${res.data.total} đ</div>
                                                                                    <div class="cart-btn">
                                                                                        <a href="/cart">Giỏ Hàng</a>
                                                                                        <a href="/checkout">Thanh Toán</a>
                                                                                    </div>
                                                                                `
                            $('.cart-info').html(html);
                            toastr('thành công', 'đã thêm vào giỏ hàng thành công', 'success');
                        } else {
                            toastr('không thành công', 'gặp 1 số lỗi vui lòng tải lại trang', 'warning');
                        }

                    }
                });
            });

            function toastr(title, mess, icon) {
                $.toast({
                    heading: title,
                    text: mess,
                    icon: icon,
                    loader: true,
                    position: 'bottom-right',
                    loaderBg: '#9EC600'
                })
            }

        </script>
@endpush
@endsection
