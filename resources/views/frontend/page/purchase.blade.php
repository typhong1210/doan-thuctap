@extends('frontend.layout.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- tab -->
                <ul class="nav nav-pills">
                    <li class="nav-item navcss active">
                        <a href="#tab1" data-toggle="tab" class="nav-link active">Tất cả</a>
                    </li>

                    <li class="nav-item navcss">
                        <a href="#tab2" data-toggle="tab" class="nav-link">Chờ xác nhận</a>
                    </li>

                    <li class="nav-item navcss">
                        <a href="#tab3" data-toggle="tab" class="nav-link">Đã xác nhận</a>
                    </li>

                    <li class="nav-item navcss">
                        <a href="#tab4" data-toggle="tab" class="nav-link ">Đang giao hàng</a>
                    </li>
                    <li class="nav-item navcss">
                        <a href="#tab5" data-toggle="tab" class="nav-link ">Đã giao hàng </a>
                    </li>
                    <li class="nav-item navcss">
                        <a href="#tab6" data-toggle="tab" class="nav-link ">Đã huỷ</a>
                    </li>
                </ul>
                <!-- Nội dung -->

                <div class="tab-content" style="border:none;padding:0px;background:none;">
                    <div class="tab-pane container active" id="tab1">
                        @foreach ($orderAll as $item)
                            @foreach ($item->products as $itemProduct)
                                @foreach ($item->orderdetails as  $itemDetail)
                                <div class="shopping-cart-item">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                            <div class="image">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                    <img class="img-reponsive img" src="{{$itemProduct->image}}" alt="{{$itemProduct->name}}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-4 c2 col-6">
                                            <div class="name">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                            </div>
                                            <div class="list-endow">
                                                <span>x {{$itemDetail->quantity}}</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                            <div class="mar-bot-mb">
                                                <span class="price price-mb" style="color: #199427;">{{number_format($itemProduct->price)}}đ</span>
                                            </div>
                                        </div>
                                        <div class="col-3 hidden-lg hidden-md"></div>
                                        {{-- <div class="col-lg-2 col-md-2 col-3">
                                            <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($itemProduct->price * $itemDetail->quantity)}}đ</p>
                                        </div> --}}
                                        <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-9"></div>
                                        <div class="col-lg-3">
                                            <div class="purchase-card-buttons__total-payment">
                                                <p>Tổng số tiền : {{number_format($itemProduct->price * $itemDetail->quantity)}}đ </p>
                                                <div class="bst">
                                                    <a href="javascript:;" data-addcart="{{ $itemProduct->id }}" class="addcart">
                                                        <button type="button" class="btn-bts btn-atc">
                                                            <i class="fa fa-shopping-cart"></i>
                                                            Mua lần nữa
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="purchase-card-buttons__show-button-wrapper">
                                                <div class="bst">
                                                    <a href="javascript:;" data-addcart="{{ $itemProduct->id }}" class="addcart">
                                                        <button type="button" class="btn-bts btn-atc">
                                                            <i class="fa fa-shopping-cart"></i>
                                                            Mua lần nữa
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane container fade" id="tab2">
                        @foreach ($orderConfirmtion as $item)
                            @foreach ($item->products as $itemProduct)
                            @foreach ($item->orderdetails as $itemDetail)
                                <div class="shopping-cart-item">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                            <div class="image">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                    <img class="img-reponsive img" src="{{$itemProduct->image}}" alt="{{$itemProduct->name}}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 c2 col-6">
                                            <div class="name">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                            </div>
                                            <div class="list-endow">
                                                <span>x {{$itemDetail->quantity}}</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                            <div class="mar-bot-mb">
                                                <span class="price price-mb" style="color: #199427;">{{number_format($itemProduct->price)}}đ</span>
                                            </div>
                                        </div>
                                        <div class="col-3 hidden-lg hidden-md"></div>
                                        <div class="col-lg-2 col-md-2 col-3">
                                            <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($itemProduct->price * $itemDetail->quantity)}}đ</p>
                                        </div>
                                        <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                                    </div>
                                </div>
                            @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane container fade" id="tab3">
                        @foreach ($orderConfirmed as $item)
                            @foreach ($item->products as $itemProduct)
                                @foreach ($item->orderdetails as $itemDetail)
                                    <div class="shopping-cart-item">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                                <div class="image">
                                                    <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                        <img class="img-reponsive img" src="{{$itemProduct->image}}" alt="{{$itemProduct->name}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 c2 col-6">
                                                <div class="name">
                                                    <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                                </div>
                                                <div class="list-endow">
                                                    <span>x {{$itemDetail->quantity}}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                                <div class="mar-bot-mb">
                                                    <span class="price price-mb" style="color: #199427;">{{number_format($itemProduct->price)}}đ</span>
                                                </div>
                                            </div>
                                            <div class="col-3 hidden-lg hidden-md"></div>
                                            <div class="col-lg-2 col-md-2 col-3">
                                                <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($itemProduct->price * $itemDetail->quantity)}}đ</p>
                                            </div>
                                            <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane container fade" id="tab4">
                        @foreach ($orderProcess as $itemOrderProcess)
                            @foreach ($itemOrderProcess->products as $orderProcessProduct)
                                @foreach ($item->orderdetails as $itemDetail)
                                    <div class="shopping-cart-item">
                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                                <div class="image">
                                                    <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                        <img class="img-reponsive img" src="{{$orderProcessProduct->image}}" alt="{{$orderProcessProduct->name}}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 c2 col-6">
                                                <div class="name">
                                                    <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                                </div>
                                                <div class="list-endow">
                                                    <span>x {{$itemDetail->quantity}}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                                <div class="mar-bot-mb">
                                                    <span class="price price-mb" style="color: #199427;">{{number_format($orderProcessProduct->price)}}đ</span>
                                                </div>
                                            </div>
                                            <div class="col-3 hidden-lg hidden-md"></div>
                                            <div class="col-lg-2 col-md-2 col-3">
                                                <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($orderProcessProduct->price * $itemDetail->quantity)}}đ</p>
                                            </div>
                                            <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane container fade" id="tab5">
                        @foreach ($orderDelivered as $item)
                            @foreach ($item->products as $itemProduct)
                            @foreach ($item->orderdetails as $itemDetail)
                                <div class="shopping-cart-item">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                            <div class="image">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                    <img class="img-reponsive img" src="{{$itemProduct->image}}" alt="{{$itemProduct->name}}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 c2 col-6">
                                            <div class="name">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                            </div>
                                            <div class="list-endow">
                                                <span>x {{$itemDetail->quantity}}</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                            <div class="mar-bot-mb">
                                                <span class="price price-mb" style="color: #199427;">{{number_format($itemProduct->price)}}đ</span>
                                            </div>
                                        </div>
                                        <div class="col-3 hidden-lg hidden-md"></div>
                                        <div class="col-lg-2 col-md-2 col-3">
                                            <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($itemProduct->price * $itemDetail->quantity)}}đ</p>
                                        </div>
                                        <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                                    </div>
                                </div>
                            @endforeach
                            @endforeach
                        @endforeach
                    </div>
                    <div class="tab-pane container fade" id="tab6">
                        @foreach ($orderCancel as $item)
                            @foreach ($item->products as $itemProduct)
                            @foreach ($item->orderdetails as $itemDetail)
                                <div class="shopping-cart-item">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                                            <div class="image">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                                    <img class="img-reponsive img" src="{{$itemProduct->image}}" alt="{{$itemProduct->name}}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 c2 col-6">
                                            <div class="name">
                                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$itemProduct->name}}"> {{$itemProduct->name}}</a>
                                            </div>
                                            <div class="list-endow">
                                                <span>x {{$itemDetail->quantity}}</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                                            <div class="mar-bot-mb">
                                                <span class="price price-mb" style="color: #199427;">{{number_format($itemProduct->price)}}đ</span>
                                            </div>
                                        </div>
                                        <div class="col-3 hidden-lg hidden-md"></div>
                                        <div class="col-lg-2 col-md-2 col-3">
                                            <p class="sub-price tt-mb" style="color: #199427;"> {{number_format($itemProduct->price * $itemDetail->quantity)}}đ</p>
                                        </div>

                                    </div>
                                </div>
                            @endforeach
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $('.addcart').on('click', function() {
                var data = $(this).data('addcart');
                var qty = $('input[name="quantity"]').val();
                $.ajax({
                    type: "POST",
                    url: "/addToCart",
                    data: {
                        dataID: data,
                        qty: qty
                    },
                    success: function(res) {
                        let html = `<small>You have
                                                                                        <em class="highlight">${res.data.count} item(s)</em> in your
                                                                                        shopping bag</small>`;
                        if (res.statusCode == 200) {
                            $.each(res.data.cart, function(i, v) {
                                html += `
                                                                                    <div class="ci-item">
                                                                                            <img src="${v.options[0]}" width="80" alt="" />
                                                                                            <div class="ci-item-info">
                                                                                                <h5>
                                                                                                    <a href="./single-product.html">${v.name}</a>
                                                                                                </h5>
                                                                                                <p>${v.qty} x ${v.price}đ</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    `
                            })
                            html += `
                                                                                    <div class="ci-total">Thành tiền: ${res.data.total} đ</div>
                                                                                    <div class="cart-btn">
                                                                                        <a href="/cart">Giỏ Hàng</a>
                                                                                        <a href="/checkout">Thanh Toán</a>
                                                                                    </div>
                                                                                `
                            $('.cart-info').html(html);
                            toastr('thành công', 'đã thêm vào giỏ hàng thành công', 'success');
                        } else {
                            toastr('không thành công', 'gặp 1 số lỗi vui lòng tải lại trang', 'warning');
                        }

                    }
                });
            });

            function toastr(title, mess, icon) {
                $.toast({
                    heading: title,
                    text: mess,
                    icon: icon,
                    loader: true,
                    position: 'bottom-right',
                    loaderBg: '#9EC600'
                })
            }

        </script>
@endpush
@endsection
