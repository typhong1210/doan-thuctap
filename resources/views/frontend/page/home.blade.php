@extends('layouts.app')
@section('content')
<div class="slider-wrap">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpeg" data-saveperformance="on" data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('images/slides/1.jpeg')}}" alt="slidebg1" data-lazyload="{{asset('images/banner1.jpg')}}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <div class="tp-caption customin fadeout tp-resizeme rs-parallaxlevel-10" data-x="center" data-y="center" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="300" style="z-index: 2; max-width: 630px; max-height: 250px; background:#fff;width:100%;height:100%; white-space: nowrap;">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="250" data-speed="1000" data-start="1400" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Raleway;
                             font-size: 36px;
                             font-weight: bold;
                             text-transform: uppercase;	color: #343434;">Amazing <span class="ss-color" style="color:#d6644a;">Outlet</span></div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="310" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Raleway;
                             font-size: 18px;
                             color: #333;text-align:center;">
                        Clean & Elegant design with a modern style. This template includes<br> all you need for a fashion & accessories store
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="375" data-speed="1000" data-start="2200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#000000;"></div>
                    <a href="./categories-grid.html" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="395" data-speed="1000" data-start="2600" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300"
                        data-endspeed="1000" style="z-index: 3; max-height:100%;line-height:43px;color:#fff;font-family: Montserrat;
                           font-size: 12px;
                           display:table;
                           font-weight: bold;
                           text-transform:uppercase;padding:0 40px;background:#000000;position:relative;z-index:77;">
                            Shop Now !
                        </a>
                </li>
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpeg" data-saveperformance="on" data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('images/slides/2.jpeg')}}" alt="slidebg1" data-lazyload="{{asset('images/banner2.jpg')}}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <div class="tp-caption customin fadeout tp-resizeme rs-parallaxlevel-10" data-x="center" data-y="center" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="300" style="z-index: 2; max-width: 630px; max-height: 250px; background:#fff;width:100%;height:100%; white-space: nowrap;">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="250" data-speed="1000" data-start="1400" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Raleway;
                             font-size: 36px;
                             font-weight: bold;
                             text-transform: uppercase;	color: #343434;">Women <span class="ss-color" style="color:#d6644a;">Clothing</span>
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="310" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Raleway;
                             font-size: 18px;
                             color: #333;text-align:center;">
                        Clean & Elegant design with a modern style. This template includes<br> all you need for a fashion & accessories store
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="375" data-speed="1000" data-start="2200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#000000;">
                    </div>
                    <a href="./categories-grid.html" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="395" data-speed="1000" data-start="2600" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300"
                        data-endspeed="1000" style="z-index: 3; max-height:100%;line-height:43px;color:#fff;font-family: Montserrat;
                           font-size: 12px;
                           display:table;
                           font-weight: bold;
                           text-transform:uppercase;padding:0 40px;background:#000000;position:relative;z-index:77;">
                            Shop Now !
                        </a>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>
<div class="featured-products">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                {{-- <h5 class="heading"><span>Sản Phẩm Nỗi Bật</span></h5> --}}
                <ul class="filter" data-option-key="filter">
                    <li><a class="dataCategory selected" data-category_id ="*" href="javascript:;">All</a></li>
                    @foreach ($menu as $item)
                        <li><a class="dataCategory" data-category_id ="{{$item->id}}" href="javascript:;">{{$item->name}}</a></li>
                    @endforeach
                </ul>
                <div id="isotope" class="isotope">
                    <div class="col-2-ct feature">
                        <div class="isotope-item">
                            <div class="product">
                                <div class="item-thumb">
                                <a href="/detail/${v.id}"><img class="image-product" src="${v.image}" class="img-responsive"/></a>
                                    <a class="overlay-rmore fa fa-search quickview" href="/detail/${v.id}"></a>
                                </div>
                                <div class="product-info">
                                    <h4 class="product-title"><a href="/detail/${v.id}">${v.name}</a></h4>
                                    <span class="product-price">${v.price}đ</em></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- POLICY -->
 <div class="policy-item parallax-bg1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-plane"></i>
                    <h4>Free shipping<span>Free shipping on all UK order</span></h4>
                    <p>Nulla ac nisi egestas metus aliquet euismod. Sed pulvinar lorem at pretium.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-money"></i>
                    <h4>Money Guarantee<span>30 days money back guarantee !</span></h4>
                    <p>Curabitur ornare urna enim, et lacinia purus tristique eulla eget feugiat diam.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-clock-o"></i>
                    <h4>Store Hours<span>Open: 9:00AM - Close: 21:00PM</span></h4>
                    <p>Etiam egestas purus eget sagittis lacinia. Morbi vel elit nec eros iaculis.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-life-ring"></i>
                    <h4>Support 24/7<span>We support online 24 hours a day</span></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit integer congue.</p>
                </div>
            </div>
        </div>
    </div>
</div>
 @if(count($topProduct) > 0)
    <div class="top-product">
        <div class="container">
            <div class="row">
                <div class="body-box-head">
                    <h5 class="heading"><span>Sản Phẩm Bán Chạy Nhất</span></h5>
                </div>
                <div id="isotope" class="isotope">
                    @foreach ($topProduct as $topproduct)
                        <div class="col-2-ct feature">
                            <div class="isotope-item">
                                <div class="product">
                                    <div class="item-thumb">
                                    <a href="{{route('home.detail',['id'=>$topproduct->id])}}"><img class="image-product" src="{{$topproduct->image}}" class="img-responsive"/></a>
                                        <a class="overlay-rmore fa fa-search quickview" href="#"></a>
                                    </div>
                                    <div class="product-info">
                                    <h4 class="product-title"><a href="/detail/${v.id}">{{$topproduct->name}}</a></h4>
                                        <span class="product-price">{{number_format($topproduct->price)}}đ</em></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
 <div class="home-blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img src="{{asset('images/banner-min.jpeg')}}" alt="">
            </div>
        </div>
    </div>
</div>
 <div class="random-product">
    <div class="container">
        <div class="row">
            <div class="body-box-head">
                <h5 class="heading"><span>Sản Phẩm Ngẫu nhiên</span></h5>
            </div>
            <div id="isotope" class="isotope">
                @foreach ($productRandom as $productrandom)
                    <div class="col-2-ct feature">
                        <div class="isotope-item">
                            <div class="product">
                                <div class="item-thumb">
                                <a href="{{route('home.detail',['id'=>$productrandom->id])}}"><img class="image-product" src="{{$productrandom->image}}" class="img-responsive"/></a>
                                    <a class="overlay-rmore fa fa-search quickview" href="#"></a>
                                </div>
                                <div class="product-info">
                                <h4 class="product-title"><a href="/detail/${v.id}">{{$productrandom->name}}</a></h4>
                                    <span class="product-price">{{number_format($productrandom->price)}}đ</em></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
 <div class="space10 clearfix"></div>
@push('scripts')
<script>
    $(document).ready(function(){

        var data = $('.dataCategory');

        data.on('click', function(){
            var categoryid = $(this).data('category_id');
            loadTabProductSelected(categoryid);
        })
        loadTabProductSelected('*')

        function loadTabProductSelected(categoryid){
            let html = '';
            $.ajax({
                url:'/product/tab',
                type: 'POST',
                data: {data:categoryid},
                success:function(data){
                    $.each(data.data,function(i,v){
                        html += `
                        <div class="col-2-ct feature">
                            <div class="isotope-item">
                                <div class="product">
                                    <div class="item-thumb">
                                    <a href="/detail/${v.id}"><img class="image-product" src="${v.image}" class="img-responsive"/></a>
                                        <a class="overlay-rmore fa fa-search quickview" href="/detail/${v.id}"></a>
                                    </div>
                                    <div class="product-info">
                                        <h4 class="product-title"><a href="/detail/${v.id}">${v.name}</a></h4>
                                        <span class="product-price">${v.price}đ</em></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                            `
                    })
                    $('#isotope').html(html);
                }
            })
        }

        $('.filter li a').click(function () {
            $('.filter li a').removeClass('selected');
            $(this).addClass('selected');
        });

    })
</script>
@endpush
@endsection

