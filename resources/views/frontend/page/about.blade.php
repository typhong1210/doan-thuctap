@extends('frontend.layout.app')
@section('content')
<div class="container">
    <section class="information">
        <div class="seo_content"></div>
        <div class="information-content">
            <div class="information-body">
                <div class="infomation-about-section">
                    <div class="row">
                        <div class="col-md-6 mb-4 d-flex">
                            <div class="about-us-content left">
                                <div>
                                    <div class="about-title"><img width="200" height="54" alt="Milem"
                                            srcset="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-story.png?v=1.3 , https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-story@2x.png?v=1.3 2x"
                                            src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-story.png?v=1.3">
                                    </div>
                                    <div class="about-text">
                                        <p style="text-align: justify;">Phát triển Smile, từ một shop online trong gần
                                            10 năm qua thành một thương hiệu uy tín nơi chị em phụ nữ có thể tìm mua
                                            được những sản phẩm chăm sóc da chính hãng cao cấp&nbsp;từ những cường quốc
                                            mỹ phẩm như Nhật Bản, Hàn Quốc, Mỹ và Châu Âu…</p>

                                        <p style="text-align: justify;">Trong đó điều cốt lõi mà Smile hướng đến là nâng
                                            cao nhận thức về dưỡng da khoa học, xóa bỏ đi những kiến thức, phương pháp
                                            truyền miệng thiếu chính xác và không căn cứ, đồng thời mang đến những sản
                                            phẩm chất lượng do chính bản thân mình đã sử dụng và kiểm nghiệm.</p>

                                        <p style="text-align: justify;">Cùng là phụ nữ, Smile&nbsp;hiểu được những khó
                                            khăn khi phải đối diện với quá nhiều thương hiệu ngoài thị trường và&nbsp;vô
                                            số lời chào mời, chỉ với mong muốn bán được sản phẩm mà không thực sự quan
                                            tâm đến những vấn đề mà bạn đang đối mặt...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <picture>
                                <source
                                    srcset="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/aboutus-1.webp?v=1.3"
                                    type="image/webp">
                                <source
                                    srcset="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/aboutus-1.png?v=1.3"
                                    type="image/png"><img width="540" height="664" alt="Milem"
                                    src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/aboutus-1.png?v=1.3">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="infomation-about-section">
                    <div class="row">
                        <div class="col-md-6 mb-4 order-md-1 order-2"><img class="" alt="Milem" width="540" height="664"
                                data-src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/aboutus-2.png?v=1.3"
                                src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/aboutus-2.png?v=1.3">
                        </div>
                        <div class="col-md-6 mb-4 order-md-2 order-1">
                            <div class="about-us-content right">
                                <div>
                                    <div class="about-title"><img width="200" height="35" class="" alt="Milem"
                                            data-srcset="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion.png?v=1.3 1x, https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion@2x.png?v=1.3 2x"
                                            data-src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion.png?v=1.3"
                                            srcset="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion.png?v=1.3 1x, https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion@2x.png?v=1.3 2x"
                                            src="https://www.milem.vn/smartcore/template/default/view/module/information/desktop/skinE/images/our-passion.png?v=1.3">
                                    </div>
                                    <div class="about-text">
                                        <p style="text-align: justify;">Smile&nbsp;mong muốn sẽ giúp bạn thực sự hiểu
                                            được làn da của mình, cảm nhận được những thay đổi và biến chuyển của làn da
                                            theo thời gian. Từ đó, Lem sẽ gợi ý cho bạn&nbsp;những quy trình dưỡng da
                                            phù hợp cũng như sử dụng những sản phẩm một cách hiệu quả nhất.&nbsp;</p>

                                        <p style="text-align: justify;">Bằng những kiến thức, kinh nghiệm và sự cố vấn
                                            từ những chuyên gia, bác sĩ da liễu hàng đầu, những quy trình và sản phẩm
                                            được tư vấn đều phải đáp ứng những tiêu chí sau: thật sự hiệu quả, mức giá
                                            đúng với chất lượng mang lại và an toàn cho người sử dụng.</p>

                                        <p style="text-align: justify;">Smile tin rằng một làn da khỏe mạnh mộc mạc sẽ
                                            giúp phái nữ&nbsp;có&nbsp;được sự tự tin, vẻ đẹp quyến rũ&nbsp;và đạt được
                                            nhiều&nbsp;thành công hơn&nbsp;trong cuộc sống. Đặc biệt, các nàng không cần
                                            phải dành quá nhiều thời gian cho việc trang điểm. Một chút phấn&nbsp;hồng,
                                            một đường kẻ mắt tinh tế cùng đôi môi đỏ căng mọng cũng đủ giúp các nàng tỏa
                                            sáng.</p>

                                        <p style="text-align: justify;">Người xưa có câu: "Nhất dáng, nhì da", điều này
                                            cho đến ngày nay vẫn không hề sai chút nào. Đối với người phụ nữ, ngoài một
                                            vóc dáng chuẩn, thì làn da khoẻ mạnh&nbsp;là vấn đề được quan tâm hơn hết
                                            thảy - và đương nhiên bạn có thể thay đổi làn da của mình ngay hôm nay.</p>

                                        <p style="text-align: justify;">Vậy thì tại sao ngay từ bây giờ chúng ta không
                                            yêu quý và chăm sóc làn da ấy?&nbsp;Mọi phụ nữ luôn xứng đáng có được một
                                            làn da khỏe mạnh, tươi đẹp và bạn cũng thế!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="text-left"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection