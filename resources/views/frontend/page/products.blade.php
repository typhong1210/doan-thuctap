@extends('frontend.layout.app')
@section('content')
@php($category = \App\Category::all())
@php($brand = \App\Manufacturers::all())
    <div class="shop-content">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 col-sm-4">
                <div class="side-widget">
                        <div class="box-left">
                            <div class="title-left">
                                <strong>DANH MỤC SẢN PHẨM</strong>
                            </div>
                            <div class="content-left">
                                <ul class="ul-non ul-left">
                                    @foreach ($menu as $categories)
                                        <li class=" ">
                                            <a href="{{ route('home.products', ['id' => $categories->id]) }}" title="Trang Điểm">
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                                {{ $categories->name }}
                                            </a>
                                            <ul class="ul-non ul-childs">
                                                @foreach ($categories->getchildcategory as $catechild)
                                                    <li>
                                                        <a href="{{ route('home.products', ['id' => $catechild->id]) }}"
                                                            title="Trang Điểm Mặt" class="">
                                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                                            {{ $catechild->name }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    <h5>Nhà cung cấp</h5>
                    <ul class="brand-list">
                        @foreach($brand as $manufactures)
                            <li><a href="{{route('home.brands',['id'=>$manufactures->id])}}">{{$manufactures->name}}</a></li>
                        @endforeach
                    </ul>
            </aside>
            <div class="col-md-9 col-sm-8">
                <div class="space50"></div>
                <div class="row">
                    @foreach($products as $product)
                    <div class="col-md-3 col-sm-6 products-show">
                        <div class="product-item">
                            <div class="item-thumb">
                                <img style="height: 200px" src="{{$product->image}}" class="img-responsive" alt="" />
                                <a href="{{route('home.detail',['id'=>$product->id])}}" class="overlay-rmore fa fa-search quickview"></a>
                            </div>
                            <div class="product-info">
                                <h4 class="product-title"><a href="./single-product.html">{{$product->name}}</a></h4>
                                <span class="product-price">{{number_format($product->price)}} <em>- Pre order</em></span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            {{$products->links()}}

            </div>
        </div>
        <div class="space50"></div>
    </div>
</div>
@endsection
