@extends('frontend.layout.app')
@section('content')
    <div class="container style">
        <div class="row">
            {{-- <h1>{{ $details['title'] }}</h1>
        <p>{{ $details['body'] }}</p> --}}
            <div class="check-out col-lg-7">
                @foreach ($errors->all() as $message)
                    <li>{{ $message }}</li>
                @endforeach
                <form method="post" action="{{ route('home.order_detail') }}">
                    @csrf
                    @method('POST')
                    <div id="load-error"></div>
                    <h2 class="h2-fiead">Địa chỉ nhận hàng</h2>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label text-rights opsize"><b>Họ và tên</b></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control opsize" id="txtName" placeholder="VD: Nguyễn Văn A (*)" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label text-rights opsize"><b>Số điện
                                thoại</b></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control opsize" id="txtPhone" placeholder="VD: 0909 099 099 (*)" name="phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label text-rights opsize"><b>Địa chỉ</b></label>
                        <div class="col-sm-9">
                            <textarea id="txtAdress" class="form-control opsize" placeholder="Số nhà, đường, phường xã..." name="address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="staticEmail" class="col-sm-3 col-form-label text-rights opsize"><b>Email</b></label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control opsize " id="txtEmail" placeholder="VD: nguyenvana@gmail.com" name="mail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label text-rights opsize"><b>Ghi chú</b></label>
                        <div class="col-sm-9">
                            <textarea id="txtNote" class="form-control opsize" placeholder="VD: Giao hàng vào buổi chiều" name="note"></textarea>
                        </div>
                    </div>
                    <div class="form-group row hidden-sm hidden-xs">
                        <div class="col-sm-6 col-7">
                            <a class="btn btn-light pull-left opsize" href="/cart">QUAY LẠI GIỎ HÀNG</a>
                        </div>
                        <div class="col-sm-6 col-5">
                            <button type="submit" class="btn btn-thanhtoan pull-right opsize" type="button" id="btn-tt">THANH TOÁN</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-5 ">
                <h2 class="title-secres">Giỏ Hàng</h2>
                <ul class="list-unstyled">
                    @foreach (\Cart::content() as $item)
                        <li>
                            <figure>
                                <a class="cart_list_product_img"
                                    href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html"
                                    title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body">
                                    <img src="{{ $item->options[0] }}" alt="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body"
                                        class="img-reponsive imds">
                                </a>
                            </figure>
                            <div class="list_content">
                                <h5>
                                    <span class="name-ps">{{ $item->name }}</span>
                                </h5>
                                <div class="quantity">
                                    {{ $item->qty }} x
                                    <span class="amount">
                                        <span class="money" style="color: #199427; font-size: 14px !important;">
                                            {{ number_format($item->price) }} ₫
                                        </span>
                                    </span>
                                    <div class="pull-right tt">
                                        {{ number_format($item->qty * $item->price) }}₫
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="load-lead">
                    <div class="box-coupon">
                        <p class="list-info-price" id="price-before" data-id="318000">
                            <small class="color-black">Tạm tính: </small> <span
                                style="color:green">{{ number_format($total), 0, '.', ',' }} ₫</span>
                        </p>
                    </div>
                </div>
                <div class="load-ship">
                    <div class="box-coupon">
                        <p class="list-info-price" id="priceship" data-id="0">
                            <small class="color-black">Phí vận chuyển: </small> <span>Miễn phí!</span>
                        </p>
                    </div>
                </div>
                <div class="subtotal">
                    Thành tiền:
                    <span>
                        <span class="amount">
                            <strong class="money lucal" style="color: #199427;"> {{ number_format($total), 0, '.', ',' }} ₫
                            </strong>
                        </span>
                    </span>
                </div>
                <div class="pttt">
                    <span>* Phương thức thanh toán: Nhận hàng &amp; thanh toán tiền mặt tại nhà</span>
                </div>
            </div>
        </div>
    </div>
@endsection
