@extends('adminlte::auth.login')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@stop
@stack('css')
@section('js')
@stack('js')
@stop
