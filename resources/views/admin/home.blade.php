@extends('adminlte::page')
@section('content')
    <div class="container">
        <h3>Hoạt động hôm nay</h3>
        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="small-box bg-info">
                    <div class="inner">
                        <p>Số đơn hàng : {{ count($orderArr) }}</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="small-box bg-gradient-success">
                    <div class="inner">
                        <p>Tiền bán hàng : {{ number_format($total) }} đ </p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-user-plus"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="info-box bg-gradient-success">
                    <span class="info-box-icon"><i class="far fa-thumbs-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                            70% Increase in 30 Days
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Đơn hàng </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <section id="tabs" class="project-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <nav>
                                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Đơn hàng trong 1 ngày</a>
                                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Đơn hàng trong 1 tuần</a>
                                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Đơn hàng trong 1 tháng</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <table class="table" cellspacing="0">
                                                    <thead>
                                                        <th>STT</th>
                                                        <th>Mã đơn hàng</th>
                                                        <th>Khách hàng</th>
                                                        <th>Status</th>
                                                        <th>Tổng giá</th>
                                                    </thead>
                                                    <tbody>
                                                        <?php  $stt = 1;?>

                                                        @foreach ($orderArr as $orderInDay)
                                                            <tr>
                                                                <td>{{$stt}}</td>
                                                                <td><a href="#">{{$orderInDay->code}}</a></td>
                                                                <td>{{$orderInDay->customerName}}</td>
                                                                <td><span class="badge badge-success">{{ $orderInDay->status }}</span></td>
                                                                <td>{{number_format($orderInDay->total)}}đ</td>
                                                            </tr>
                                                            <?php $stt++;?>

                                                        @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                <table class="table" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Mã đơn hàng</th>
                                                            <th>Khách hàng</th>
                                                            <th>Status</th>
                                                            <th>Tổng giá</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php  $stt = 1;?>

                                                        @foreach ($orderByWeek as $orderInWeek)
                                                            <tr>
                                                                <td>{{$stt}}</td>
                                                                <td><a href="#">{{$orderInWeek->code}}</a></td>
                                                                <td>{{$orderInWeek->customerName}}</td>
                                                                <td><span class="badge badge-success">{{ $orderInWeek->status }}</span></td>
                                                                <td>{{number_format($orderInWeek->total)}}đ</td>
                                                            </tr>
                                                            <?php $stt++;?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                                <table class="table" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Mã đơn hàng</th>
                                                            <th>Khách hàng</th>
                                                            <th>Status</th>
                                                            <th>Tổng giá</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php  $stt = 1;?>
                                                        @foreach ($orderByMonth as $orderInMonth)
                                                            <tr>
                                                                <td>{{$stt}}</td>
                                                                <td><a href="#">{{$orderInMonth->code}}</a></td>
                                                                <td>{{$orderInMonth->customerName}}</td>
                                                                <td><span class="badge badge-success">{{ $orderInMonth->status }}</span></td>
                                                                <td>{{number_format($orderInMonth->total)}}đ</td>
                                                            </tr>
                                                            <?php $stt++;?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="card-footer clearfix">
                        <a href="/order" class="btn btn-sm btn-secondary float-right">Xem tất cả đơn hàng</a>
                      </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Recently Added Products</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <ul class="products-list product-list-in-card pl-2 pr-2">
                            @foreach ($products as $itemProduct)
                                <li class="item">
                                    <div class="product-img">
                                        <img src="{{ $itemProduct->image }}" alt="Product Image" class="img-size-50">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title">
                                            @foreach ($itemProduct->categories as $item)
                                                {{ $item->name }}
                                            @endforeach
                                            <span
                                                class="badge badge-warning float-right">{{ number_format($itemProduct->price) }}đ</span>
                                        </a>
                                        <span class="product-description">
                                            {{ $itemProduct->name }}
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-footer text-center">
                        <a href="/product" class="uppercase">View All Products</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
