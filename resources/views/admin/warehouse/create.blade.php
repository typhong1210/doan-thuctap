@extends('adminlte::page')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

@endpush
@section('title', 'Tao san pham moi')

@section('content')

    <div class="container-fuild">
        <form method="post" action="{{ route('warehouse.store') }}">
            @csrf
            <input type="hidden" name="product_id">
            <div class="row">
                <div class="col-md-8">
                    <select class="select2" style="width: 100%" type="text">
                        @foreach ($products as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                    <th class="text-center">STT</th>
                                    <th class="text-center">Mã SP</th>
                                    <th class="text-center">Tên SP</th>
                                    <th class="text-center">Hình ảnh</th>
                                    <th class="text-center">SL</th>
                                    <th class="text-center">Giá nhập</th>
                                    <th class="text-center">Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody id="render">

                                </tbody>
                            </table>
                        </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Nhà cung cấp</label>
                        <select name="manufacturer_id" class="form-control">
                            @forelse($manufacturers as $manufacturer)
                                <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                            @empty
                                <option>No data</option>
                            @endforelse
                        </select>
                    </div>
                        <div class="form-group">
                            <label for="">Ngày nhập</label>
                            <input type="text" class="form-control" name="date">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Người nhập</label>
                            <select name="user_id" class="form-control">
                                @forelse($user as $users)
                                    <option value="{{ $users->id }}">{{ $users->name }}</option>
                                @empty
                                    <option>No data</option>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Ghi chú</label>
                            <input type="text" class="form-control" name="note">
                        </div>
                        <hr>
                        <div class="totalAll"></div>
                </div>
                <button type="submit" class="btn btn-success">Lưu</button>

            </div>
        </form>
    </div>
@stop
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        var product_id = [];
         $('.select2').select2({
            selectOnClose: true
        });
        $('.select2').on('select2:select', function (e) {
        var data = e.params.data.id;
        // console.log(data);
        $.ajax({
            type: "POST",
            url: "/warehouse/render",
            data: {data:data},
            success: function (res) {
                if(res.statusCode == 200){
                    // alert(res);
                    var html = `
                            <tr>
                                <td>1</td>
                                <td>SP0000${res.data.id}</td>
                                <td>${res.data.name}</td>
                                <td><img style="width:30px" src="/storage/${res.data.image}" alt=""></td>
                                <td><input style="min-width:55px;max-height: 34px;" class="form-control changeQuantity" type="text" value="1"></td>
                                <td class="price" data-price=${res.data.price}>${res.data.price}</td>
                                <td class="totalPrice" data-total="${res.data.price * 1}">${res.data.price * 1}</td>
                            </tr>
                        `;
                            updateQuantity(res.data);
                    console.log(product_id);
                    $('#render').append(html);
                }

            }
        });

        $(document).on('change keyup','.changeQuantity',function(){
            var quantity = $(this).val();
            var price = $(this).parent().parent().find('.price').data('price');
            // $(this).parent().parent().find('.totalPrice');
            $(this).parent().parent().find('.totalPrice').attr('data-total',updateTotal(quantity,price))
            .html(updateTotal(quantity,price));
            total();
        });
        function updateQuantity(data){
            var product = {
                            'id': res.data.id,
                            'totalprice': total(),
                            'quantity': $('.changeQuantity').val()
            }
            product_id.push(product);
            $('input:hidden[name="product_id"]').val(JSON.stringify(product_id));
        }
        function updateTotal(quantity,price){
            return quantity * price;
        }
        function total(){
            var total = 0;
            var totalAttr = $('.totalPrice');
            $.each(totalAttr, function (indexInArray, valueOfElement) {
                var num = $(this).attr('data-total');
                if(!isNaN($(this).val())){
                    total += parseInt(num);
                }
            });
            $('.totalAll').html(`tong tien : ` + total);
        }
    });
    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    </script>
{{-- <script>
    $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
    var product_id = [];
    // $('.select2').select2({
    //     selectOnClose: true
    // });
    $('.select2').on('select2:select', function (e) {
        var data = e.params.data.id;
        // console.log(data);
        $.ajax({
            type: "POST",
            url: "/warehouse/render",
            data: {data:data},
            success: function (res) {
                if(res.statusCode == 200){
                    var html = `
                            <tr>
                                <td>1</td>
                                <td>SP0000${res.data.id}</td>
                                <td>${res.data.name}</td>
                                <td><img style="width:30px" src="/storage/${res.data.image}" alt=""></td>
                                <td><input style="min-width:55px;max-height: 34px;" class="form-control changeQuantity" type="text" value="1"></td>
                                <td class="price" data-price=${res.data.price}>${res.data.price}</td>
                                <td class="totalPrice" data-total="${res.data.price * 1}">${res.data.price * 1}</td>
                            </tr>
                        `;
                        // var product = {
                        //     'id': res.data.id,
                        //     'totalprice': res.data.totalprice,
                        //     'quantity': $('.changeQuantity').val();
                        // }
                    // product_id.push(product);
                    // // product_id.element['quantity'].push(res.data.quantity);
                    // // product_id.element['totalprice'].push(res.data.totalprice);
                    // $('input:hidden[name="product_id"]').val(JSON.stringify(product_id));
                    // $('#render').append(html);
                }

            }
        });

        $(document).on('change keyup','.changeQuantity',function(){
            var quantity = $(this).val();
            var price = $(this).parent().parent().find('.price').data('price');
            // $(this).parent().parent().find('.totalPrice');
            $(this).parent().parent().find('.totalPrice').attr('data-total',updateTotal(quantity,price))
            .html(updateTotal(quantity,price));
            total();
        });
        function updateTotal(quantity,price){
            return quantity * price;
        }
        function total(){
            var total = 0;
            var totalAttr = $('.totalPrice');
            $.each(totalAttr, function (indexInArray, valueOfElement) {
                var num = $(this).attr('data-total');
                if(!isNaN($(this).val())){
                    total += parseInt(num);
                }
            });
            $('.totalAll').html(`tong tien : ` + total);
        }
    });
</script> --}}
@endpush


