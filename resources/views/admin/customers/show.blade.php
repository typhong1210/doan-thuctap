@extends('adminlte::page')
@section('content')
<h1 style="text-align: center"></h1>
@foreach ($orders as $item)
<div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">Tên Khách Hàng: {{$customer->name}} </h5>
      <p class="card-text">Mã Đơn Hàng: {{$item->ordernumber}}</p>
    </div>
</div>
@endforeach
<table class="table">
    <thead>
      <tr>
        <th scope="col">STT</th>
        <th scope="col">Tên Sản Phẩm</th>
        <th scope="col">Hình Ảnh</th>
        <th scope="col">Số Lượng</th>
        <th scope="col">Đơn Giá</th>
        <th scope="col">Tổng Giá</th>
      </tr>
    </thead>
    <?php  $stt = 1;?>
    <tbody>
        @foreach ($orderDetails as $orderdetail)
        <tr>
            <td>{{$stt}}</td>
            <td>{{$orderdetail->productName}}</td>
            <td><img width="120" src="{{asset('storage/'.str_replace('public','',$orderdetail->productImage))}}" alt=""></td>
            <td>{{$orderdetail->quantity}}</td>  
            <td>{{number_format($orderdetail->price)}}</td>        
            <td>{{number_format($orderdetail->total)}}</td> 
        </tr>
        <?php $stt++;?>
        @endforeach
    </tbody>
  </table>
@endsection