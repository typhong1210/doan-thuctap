@extends('adminlte::page')
@section('title', 'Danh Sách Khách Hàng')
@section('content')

    <h1 style="text-align: center ">Danh Sách Khách Hàng</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Họ Và Tên</th>
            <th scope="col">Địa Chỉ</th>
            <th scope="col">Số Điện Thoại</th>
            <th scope="col">Ghi Chú</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
        @forelse($customers as $customer)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a href="{{route('customer.show',['id'=>$customer->id])}}">{{$customer->name}}</a></td>
                <td>{{$customer->address}}</td>
                <td>{{$customer->phone}}</td>
                <td>{{$customer->note}}</td>
                <td><a href="{{ route('customer.destroy', ['id'=>$customer->id]) }}">Xoá</a></td>
            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop

