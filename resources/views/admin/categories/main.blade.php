@extends('adminlte::page')
@section('title', 'Danh sach the loai')
@section('content')
    <div style="width:100%;display:flex;" class="title-page">
        <p style="width: 89%">Danh mục</p>
        <a href="/category/create"><button class="btn btn-success">Tạo danh mục</button></a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
        @forelse($categories as $c)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a href="{{route('category.edit',['id'=>$c->id])}}">{{$c->name }}</a></td>
                <td><a href="{{ route('category.destroy',['id'=>$c->id]) }}">Delete</a></td>
            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop

