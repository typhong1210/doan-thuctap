@extends('adminlte::page')
@section('title', 'Danh sach the loai')
@section('content')
    <div style="width:100%;display:flex;" class="title-page">
        <p style="width: 89%">Nhà cung cấp </p>
        <a href="/category/create"><button class="btn btn-success">Tạo Mới</button></a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php  $stt = 1;?>
        @forelse($manufacturers as $mn)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a href="{{route('manufacturers.edit',['id'=>$mn->id])}}">{{$mn->name }}</a></td>
                <td><a href="{{ route('manufacturers.destroy',['id'=>$mn->id]) }}">Delete</a></td>
            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop

