@extends('adminlte::page')
@section('content')
    <form method="POST" action="{{route('admin.updateOrder',['id'=>$order->id])}}">
        @csrf
      <div class="card">
        <div class="card-header">
          <h2 class="card-title">Chi tiết đơn hàng</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <tbody>
                    <tr class="table-danger">
                        <th style="width: 40%;">Khách hàng</th>
                        <td>{{$order->customer->name}}</td>
                    </tr>
                    <tr >
                        <th style="width: 40%;">Số điện thoại</th>
                        <td>{{$order->customer->phone}}</td>
                    </tr>
                    <tr class="table-danger">
                        <th style="width: 40%;">Email</th>
                        <td>{{$order->customer->mail}}</td>
                    </tr>
                    <tr >
                        <th style="width: 40%;">Địa chỉ</th>
                        <td>{{$order->customer->address}}</td>
                    </tr>
                    <tr class="table-danger" >
                        <th style="width: 40%;">Ghi chú</th>
                        <td>{{$order->customer->note}}</td>
                    </tr>
                    <tr>
                        <th style="width: 40%;">Ngày tạo</th>
                        <td>{{$order->created_at}}</td>
                    </tr>
                    <tr class="table-danger" >
                        <th style="width: 40%;">Trạng thái</th>
                        <td>
                            <div class="form-group">
                                <select name="status" class="form-select" aria-label="Default select example">
                                    {{-- <option  selected>{{$order->status}}</option> --}}
                                    <option {{$order->status == 1 ? "selected" : ""}} value="1">Chưa Xác Nhận</option>
                                    <option {{$order->status == 2 ? "selected" : ""}} value="2">Đã Xác Nhận</option>
                                    <option {{$order->status == 3 ? "selected" : ""}} value="3">Đang Giao</option>
                                    <option {{$order->status == 4 ? "selected" : ""}} value="4">Đã Giao</option>
                                    <option {{$order->status == 5 ? "selected" : ""}} value="5">Đã Huỷ</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
      </div>
      <div class="card ">
        <div class="card-header">
          Thông tin sản phẩm
        </div>
        <div class="card-body">
          <table class="table">
            <thead>
                <th>Sản phẩm</th>
                <th>Hình ảnh</th>
                <th>Giá</th>
                <th>Số lượng</th>
                <th>Tổng</th>
            </thead>
            <tbody>
                @foreach ($order->orderdetails as $itemProduct)
                    <tr class="table-danger">
                        <td>{{$itemProduct->products->name}}</td>
                        <td><img width="100" height="60" src="{{$itemProduct->products->image}}" alt=""></td>
                        <td>{{number_format($itemProduct->products->price)}}</td>
                        <td>{{$itemProduct->quantity}}</td>
                        <td>{{number_format($itemProduct->total)}} đ</td>
                    </tr>
                @endforeach
                <tr class="table-danger">
                    <th colspan="3">Tổng tiền</th>
                    <td></td>
                    <td>{{number_format($order->total)}} đ</td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
      <input type="submit" value="Submit">
    </form>
@endsection
@push('css')
    <style>
        .card-body{
            padding:0px;
        }
        .table td, .table th,.table thead th {
            border-top:none;
            border-bottom:none;
        }
    </style>
@endpush
