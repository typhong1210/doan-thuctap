@extends('adminlte::page')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Đơn hàng</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>STT</th>
                    <th>Mã code</th>
                    <td>Trạng thái</td>
                    <th>Khách hàng</th>
                    <th>Số điện thoại</th>
                    <th>Tổng giá</th>
                    <th>Ngày tạo</th>
                    <th></th>
                  </tr>
                </thead>
                <?php  $stt = 1;?>

                <tbody>
                    @foreach ($orders as $item)
                    <tr data-widget="expandable-table" aria-expanded="false">
                        <td>{{$stt}}</td>
                        <td><a href="#">  {{$item->code}}</a></td>

                        <td>
                            @if($item['status'] == 1)
                                <a style="color: white;font-size: 10px"
                                class="btn btn-info btn-xs"><i class="fas fa-hand-paper"></i> Chờ Xác Nhận</a>
                            @elseif($item['status'] == 2)
                                <a style="color: white;font-size: 10px"
                                class="btn btn-secondary btn-xs"><i class="fas fa-hand-paper"></i> Đã Xác Nhận</a>
                            @elseif($item['status'] == 3)
                                <a style="color: white;font-size: 10px"
                               class="btn btn-warning btn-xs"><i class="fas fa-shipping-fast"></i> Đang giao ...</a>
                            @elseif($item['status'] == 4)
                                <a style="color: white;font-size: 10px"
                                class="btn btn-success btn-xs"><i class="fas fa-check-circle"></i> Đã giao</a>
                            @elseif($item['status'] == 5)
                                <a style="color: white;font-size: 10px"
                                class="btn btn-danger btn-xs"><i class="fas fa-ban"></i> Đã Huỷ</a>
                            @endif
                        </td>
                        <td>{{$item->customer->name}}</td>
                        <td>{{$item->customer->phone}}</td>
                        <td>{{number_format($item->total)}}đ</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            <a href="{{route('admin.orderDetail',['id'=>$item->id])}}"><i class="fa fa-eye"></i></a>
                            <a href="{{route('admin.editOrder',['id'=>$item->id])}}"><i class="fa fa-edit"></i></a>
                        </td>
                        <?php $stt++;?>
                    @endforeach
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
</div>
@endsection
