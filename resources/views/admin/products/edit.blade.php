@extends('adminlte::page')

@section('title', 'Tao san pham moi')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
    <h1 style="text-align: center">Chỉnh sửa sản phẩm</h1>
    <h1>{{ (session('message') ? session('message') : " ") }}</h1>
    <div class="error">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <form method="post" action="{{ route('product.update',['id'=>$product->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1">Tên SP</label>
            <input type="text" value="{{$product->name}}" name="name" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER NAME">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Giá vốn</label>
            <input type="text" value="{{$product->price}}" name="price" class="form-control" placeholder="Price">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Số lượng</label>
            <input type="number" value="{{$product->quantity}}" name="quantity" class="form-control" placeholder="Số lượng">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Hình ảnh</label>
            <input type="hidden" name="imageurl" value="{{$product->image}}" class="form-control-file">
            <input type="file" name="image" class="form-control-file">
            <div>
                <img width="120" src="{{$product->image}}">
            </div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Mô tả ngắn</label>
            <textarea name="description" rows="4" cols="50" class="form-control" placeholder="Mô tả">{{$product->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Mô tả Dài</label>
            <textarea name="parent_description" class="form-control" placeholder="Mô tả">{{$product->parent_description}}</textarea>
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Nhà cung cấp</label>
            <select name="manufacturer_id" class="form-control" placeholder="manufacturer_id">
                @forelse($manufacturers as $mn)
                    <option  value="{{ $mn->id }}">{{ $mn->name }}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Danh mục</label>
            <select name="category_id[]" multiple="multiple" class="form-control select2">
                @forelse($categories as $category)
                    <option value="{{$category->id}}" {{(in_array($category->id, $catebyProduct)) ? "selected" : ""}}>{{$category->name}}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Cập nhập</button>
    </form>
@stop

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
    <script>
        $('.select2').select2({
            selectOnClose: true
        });
        CKEDITOR.replace( 'description' );
        CKEDITOR.replace( 'parent_description' );
    </script>

@endpush
