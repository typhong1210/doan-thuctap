@extends('adminlte::page')
@section('title', 'Danh sach san pham')
@section('content')
    <div style="width:100%;display:flex;" class="title-page">
        <p style="width: 89%">Danh sách sản phẩm</p>
        <a href="/product/create"><button class="btn btn-success">Tạo sản phẩm</button></a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Tên SP</th>
            <th scope="col">Giá vốn</th>
            <th scope="col">Hình ảnh</th>
            <th scope="col">Số lượng</th>
            <th scope="col">Nhà cung cấp</th>
            <th scope="col">Danh mục</th>
            {{-- <th scope="col">Type</th> --}}
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>

        @forelse($products as $pr)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a href="{{route('product.edit',['id'=>$pr->id])}}">{{$pr->name }}</a></td>
                <td>{{$pr->price }}</a></td>
                <td><img width="120" src="{{$pr->image}}" /></td>
                <td>{{$pr->quantity}}</td>
                <td>{{ $pr->manufactures->name }}</td>
                <td>
                    @if ($pr->categories->isEmpty())
                        {{'abc'}}
                    @else
                        @foreach ($pr->categories as $item)
                            <span class="badge badge-pill badge-primary">{{$item->name}}</span>
                        @endforeach
                    @endif
                </td>
                <td><a href="{{ route('product.destroy',['id'=>$pr->id])}}">Delete</a></td>
            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop
