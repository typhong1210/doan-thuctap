<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Phap',
                'email' => 'admin123@gmail.com',
                'password' => bcrypt('admin123'),
                'is_admin' => 1,
            ]
        ]);
    }
}
