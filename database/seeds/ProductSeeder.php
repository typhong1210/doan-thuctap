<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Serum The Ordinary Niacinamide 10% + Zinc 1%',
                'price' => '300000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620205710/Storage/zkggdpfijaubv9bgeufi.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>-Kh&aacute;ng vi&ecirc;m, giảm k&iacute;ch ứng cho da mụn.</p>
                <p>-Kiểm so&aacute;t dầu, giảm b&oacute;ng nhờn da.</p>

                <p>-Thu nhỏ lỗ ch&acirc;n l&ocirc;ng, giảm t&igrave;nh trạng tắc nghẽn lỗ ch&acirc;n l&ocirc;ng.</p>

                <p>-Dưỡng ẩm, nu&ocirc;i dưỡng da khỏe.</p>
                ',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SERUM TRỊ MỤN THE ORDINARY NIACINAMIDE 10% + ZINC 1%</h2>

                <p>The Ordinary-Một thương hiệu mỹ phẩm đang &acirc;m thầm được c&aacute;c Beauty blogger kh&ocirc;ng chỉ ở Việt Nam m&agrave; to&agrave;n thế giới ch&uacute; &yacute;. V&agrave; đương nhi&ecirc;n,l&agrave; một c&ocirc; n&agrave;ng đam m&ecirc; skincare của Beauty Garden sẽ kh&ocirc;ng thể bỏ qua c&aacute;c sản phẩm của The Ordinary. Thương hiệu hiếm hoi về chất lượng sản phẩm hiệu quả cao m&agrave; gi&aacute; th&agrave;nh rất chi phải chăng.</p>

                <p>Đi theo xu hướng tối giản, sử dụng một m&agrave;u trắng v&agrave; x&aacute;m sạch sẽ v&agrave; thể hiện t&iacute;nh khoa học trong từng sản phẩm-The Ordinary dễ d&agrave;ng được mọi người đ&oacute;n nhận.</p>

                <p>Tinh chất trị mụn v&agrave; th&acirc;m The Ordinary Niacinamide 10% + Zinc 1% nghe t&ecirc;n thấy xa lạ nhưng nếu bạn quyết định để em ấy trở th&agrave;nh &ldquo;cạ cứng&rdquo; th&igrave; sẽ kh&ocirc;ng hối hận đ&acirc;u nh&eacute;, nhất l&agrave; những n&agrave;ng vừa trải qua một đợt trị mụn, phải dọn dẹp &ldquo;b&atilde;i chiến trường&rdquo; đầy th&acirc;m, sẹo v&agrave; mấy t&ecirc;n đ&egrave;n pin li ti c&ograve;n s&oacute;t lại.</p>

                <p><strong>The Ordinary Niacinamide 10% + Zinc 1%</strong>, nghe t&ecirc;n đ&atilde; biết đến th&agrave;nh phần v&agrave; liều lượng c&oacute; trong sản phẩm. Nếu Zinc kh&aacute; quen thuộc với chức năng c&acirc;n bằng b&atilde; nhờn tr&ecirc;n da, l&agrave;m săn chắc da, s&aacute;t khuẩn nhẹ, l&agrave;m dịu tổn thương cũng như hỗ trợ trị mụn nhẹ th&igrave; Niacinamide vẫn l&agrave; một &ldquo;nh&acirc;n tố b&iacute; ẩn&rdquo;.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2018/10/8/a6090830.jpg" /></p>
                ',
            ],
            [
                'name' => 'Bông Tẩy Trang Cotton Ipek Cotton Pads',
                'price' => '26000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620622833/Storage/m61ondmtfclgyzuzvbtu.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Với c&ocirc;ng nghệ sản xuất b&ocirc;ng ti&ecirc;n tiến, b&ocirc;ng tẩy trang IPEK gi&uacute;p tẩy trang v&agrave; l&agrave;m sạch mọi bụi bẩn b&aacute;m tr&ecirc;n da.</p>

                <p>- Miếng b&ocirc;ng được dệt th&agrave;nh 2 mặt kh&aacute;c nhau gi&uacute;p tận dụng được tối đa sản phẩm.</p>

                <p>- Sợi b&ocirc;ng mềm mịn thấm h&uacute;t s&acirc;u dung dịch, nhẹ nh&agrave;ng tẩy sạch v&ugrave;ng da nhạy cảm quanh mắt.</p>

                <p>- Tẩy sạch s&acirc;u l&agrave;n da m&agrave; tuyệt đối kh&ocirc;ng để lại xơ, cho l&agrave;n da s&aacute;ng mịn, rạng ngời.</p>
                ',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p><strong><a href="http://beautygarden.vn/Bong-tay-trang-cotton-lpek-cotton-pads-1253-0-157-5.html">B&ocirc;ng tẩy trang Ipek&nbsp;</a></strong>c&oacute; xuất xứ từ Thổ Nhĩ Kỳ được sản xuất tự sợi b&ocirc;ng cải tiến, với th&agrave;nh phần 100% cotton tự nhi&ecirc;n sẽ hỗ trợ bạn tẩy sạch mọi bụi bẩn v&agrave; cặn trang điểm tr&ecirc;n da. Đặc biệt sản phẩm c&oacute; lớp b&ocirc;ng mền mịn thấp h&uacute;t cao, lau nhẹ nh&agrave;ng v&agrave; kh&ocirc;ng để lại sợi b&ocirc;ng b&aacute;m tr&ecirc;n da, b&ocirc;ng dầy v&agrave; d&ugrave;ng được 2 mặt gi&uacute;p bạn tiết kiệm tốt đa chi ph&iacute; sử dụng với sản phẩm n&agrave;y.</p>

                <p>Ipek l&agrave; thương hiệu b&ocirc;ng tẩy trang hữu cơ được sản xuất bởi c&ocirc;ng ty Ipek Idrofil Pamuk &ndash; C&ocirc;ng ty h&agrave;ng đầu tại Đ&ocirc;ng &Acirc;u về sản xuất b&ocirc;ng an to&agrave;n, được th&agrave;nh lập từ năm 1971. Sản phẩm của Ipek được sản xuất theo ti&ecirc;u chuẩn dược phẩm ch&acirc;u &Acirc;u với c&aacute;c chứng chỉ chất lượng ISO 9001; 2000, ECOCERT v&agrave; được tin d&ugrave;ng ở nhiều nước tr&ecirc;n thế giới</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/bong-tay-trang-ipek.jpg" /></p>
                ',
            ], [
                'name' => 'Kem Chống Nắng Innisfree Intensive Long-Lasting',
                'price' => '250000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620205020/Storage/wjuc1hdm8qg9uosmjn2z.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Chiết xuất từ rau m&aacute; v&agrave; tr&agrave; xanh gi&uacute;p cung cấp độ ẩm v&agrave; l&agrave;m dịu da hiệu quả.</p>

                <p>- Thẩm thấu nhanh, tạo cảm gi&aacute;c kh&ocirc; tho&aacute;ng dễ chịu.</p>

                <p>- Bảo vệ da ho&agrave;n hảo dưới t&aacute;c động của &aacute;nh nắng mặt trời g&acirc;y hại.</p>

                <p>- Khả năng chống thấm nước cao c&oacute; t&aacute;c dụng bảo vệ da suốt thời gian d&agrave;i</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN KEM CHỐNG NẮNG INNISFREE INTENSIVE LONG-LASTING SUNSCREEN SPF50+/PA++++ 50ML #FOR-OILY-SKIN</h2>

                <p><strong>Intensive Long Lasting Sunscreen Spf 50 Pa++++ 50ml</strong>&nbsp;l&agrave; kem chống nắng lai h&oacute;a học v&agrave; vật l&yacute; mới ra mắt phi&ecirc;n bản mới v&agrave;o đầu năm 2019 của Innisfree. Với chiết xuất từ tinh dầu hoa hướng dương v&agrave; l&aacute; tr&agrave; xanh từ đảo Jeju bảo vệ da khỏe mạnh khỏi tia UV v&agrave; m&ocirc;i trường c&oacute; hại kh&aacute;c trong nhiều giờ m&agrave; kh&ocirc;ng bị tr&ocirc;i bởi mồ h&ocirc;i hay b&atilde; nhờn tr&ecirc;n da.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2019/3/30/a1101541.jpg" style="height:400px; width:650px" /></p>',
            ], [
                'name' => 'Nước Hoa Hồng Simple Soothing Facial Toner',
                'price' => '85000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117254/Storage/whhigmlqwab6a7uit7wr.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Nước hoa hồng Simple Soothing Facial Toner l&agrave; d&ograve;ng Toner lu&ocirc;n đứng trong top &ldquo;Best seller&rdquo; của h&atilde;ng Simple.</p>

                <p>- Nổi tiếng bởi khả năng l&agrave;m sạch s&acirc;u, nu&ocirc;i dưỡng v&agrave; c&acirc;n bằng độ PH tốt nhất cho da</p>

                <p>- Sản phẩm ho&agrave;n to&agrave;n l&agrave;nh t&iacute;nh v&agrave; dịu nhẹ cho l&agrave;n da của bạn.</p>

                <p>- Gi&uacute;p bạn chăm s&oacute;c l&agrave;n da của m&igrave;nh một c&aacute;ch to&agrave;n diện nhất.</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN VỀ NƯỚC HOA HỒNG SIMPLE SOOTHING FACIAL TONER</h2>

                <p><a href="http://beautygarden.vn/nuoc-hoa-hong-simple-sensitive-skin-experts-soothing-facial-toner.html"><strong>Nước hoa hồng Simple Kind To Skin Soothing Facial Toner&nbsp;</strong></a>l&agrave; một sản phẩm chăm s&oacute;c da đến từ Anh Quốc với dung t&iacute;ch 200ml được đ&aacute;nh gi&aacute; rất cao 4,5/5* tr&ecirc;n trang makeupalley v&agrave; thường xuy&ecirc;n nằm trong top c&aacute;c sản phẩm drugstore chất lượng, đ&aacute;ng đồng tiền. Kh&ocirc;ng những vậy, ở Vi&ecirc;t Nam Simple c&ograve;n được c&aacute;c Beauty Bloger nhắc đến kh&aacute; nhiều bởi khả năng l&agrave;m sạch, nu&ocirc;i dưỡng cho da m&agrave; kh&ocirc;ng hề g&acirc;y k&iacute;ch ứng. Bạn n&agrave;y th&iacute;ch hợp mọi loại da, ngay cả da nhạy cảm n&ecirc;n bạn ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m c&oacute; thể rinh bạn ấy về với đội của bạn.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/nuoc-hoa-hong-simple-soothing-facial-toner-0-hinh-anh-1.jpg" /></p>

                <p>&nbsp;</p>',
            ], [
                'name' => 'Tinh Chất Dưỡng Da Caryophy Portulaca Ampoule',
                'price' => '350000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117223/Storage/xxztm9gddoevbbgtamll.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '- Tinh chất dưỡng da Caryophy giúp giữ ẩm cho da, dưỡng da mềm mại mịn màng đầy sức sống. ',
                'parent_description' => 'THÔNG TIN VỀ TINH CHẤT DƯỠNG DA CARYOPHY PORTULACA AMPOULE
                Một làn da đẹp luôn là niềm mong muốn của rất nhiều cô gái, thế nhưng không phải ai cũng may mắn sở hữu một làn da đẹp, trắng sáng mịn màng không tỳ vết. Dưới tác động của môi trường sống ô nhiễm hàng ngày, làn da chúng ta gặp phải vô vàn vấn đề không mong muốn. 

                Tinh Chất Dưỡng Da Caryophy Portulaca Ampoule với thành phần chiết xuất từ thiên nhiên, có khả năng thẩm thấu nhanh chóng - là một trong những dòng sản phẩm dưỡng da được yêu thích nhất tại Hàn Quốc hiện nay. Ngay khi tiếp xúc với da, hoạt chất trong sản phẩm sẽ thấm sâu vào da, cung cấp nước và dưỡng chất cần thiết cho da, giúp da khỏe mạnh từ bên trong, sáng mịn và mẩm mượt đầy sức sống. 
                ',
            ], [
                'name' => 'Thạch Hoa Hồng Dưỡng Ẩm Cocoon',
                'price' => '375000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620205054/Storage/dtjkvpt6mq0tx02pvfam.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>Với kết cấu mọng nước mang nhiều dưỡng chất từ nước hoa hồng hữu cơ kết hợp với nam châm dưỡng ẩm Pentavitin, các axit amin và HA, thạch hoa hồng sẽ nuôi dưỡng và khóa ẩm suốt 24 giờ, mang lại làn da đầy đặn, mềm mượt và mịn màng.</p> ',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>Tất cả ch&uacute;ng ta đều biết rằng đối ph&oacute; với l&agrave;n mất nước l&agrave; một cuộc chiến kh&oacute; khăn, v&agrave; ch&uacute;ng t&ocirc;i tin rằng việc cấp ẩm cho da l&agrave; một việc l&agrave;m quan trọng v&agrave; ti&ecirc;n quyết để c&oacute; 1 l&agrave;n da khỏe mạnh.<br />
                Trong qu&aacute; tr&igrave;nh t&igrave;m kiếm c&aacute;c nguy&ecirc;n liệu để kết hợp c&ugrave;ng hoa hồng hữu cơ Cao Bằng, b&ecirc;n cạnh HA v&agrave; c&aacute;c axit amin, ch&uacute;ng t&ocirc;i đ&atilde; nghi&ecirc;n cứu v&agrave; thấy rằng, hoạt chất Pentavitin l&agrave; sự lựa chọn ph&ugrave; hợp với sản phẩm nu&ocirc;i dưỡng v&agrave; cấp ẩm cho l&agrave;n da.<br />
                Pentavitin &ndash; hoạt chất ưa nước với t&iacute;nh năng &ldquo;hydrat h&oacute;a&rdquo; mạnh mẽ như một nam ch&acirc;m, gi&uacute;p c&aacute;c tế b&agrave;o da ngậm nước nhanh ch&oacute;ng v&agrave; tạo lớp m&agrave;ng kh&oacute;a ẩm ho&agrave;n hảo l&ecirc;n đến 24H. Đặc biệt, hoạt chất n&agrave;y kh&oacute; bị rửa tr&ocirc;i bởi nước, một ưu điểm m&agrave; kh&ocirc;ng phải nguy&ecirc;n liệu dưỡng ẩm n&agrave;o cũng c&oacute; được. Tất cả những c&ocirc;ng dụng tuyệt vời n&agrave;y được ch&uacute;ng t&ocirc;i mang đến trong sản phẩm THẠCH HOA HỒNG DƯỠNG ẨM</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2021/3/4/120371757_3326117074150562_8373812695080043048_o105511.jpg" style="height:960px; width:960px" /></p>',
            ], [
                'name' => 'Kem Dưỡng Ẩm Cừu Tím Lanolin Cream Careline',
                'price' => '90000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117306/Storage/bkrjquw60zl8sre70o7u.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p><p>- Kem cừu careline chứa tinh chất dầu hạt nho v&agrave; bổ sung th&ecirc;m vitamine E</p>

                <p>- Cung cấp đầy đủ dưỡng chất nu&ocirc;i dưỡng l&agrave;n da chống lại sự l&atilde;o h&oacute;a, ngăn ngừa chảy xệ</p>

                <p>- Gi&uacute;p l&agrave;n da đ&agrave;n hồi tốt hơn, giảm thiểu c&aacute;c nếp nhăn tr&ecirc;n mặt.</p>

                <p>- Kem cừu Careline khi thoa l&ecirc;n mặt kh&ocirc;ng bị nhờn b&oacute;ng tr&ecirc;n da, tạo cảm gi&aacute;c săn chắc, dễ chịu.</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p><a href="http://www.beautygarden.vn/kem-duong-am-va-san-chac-da-lanolin-cream-careline.html"><strong>Kem dưỡng ẩm v&agrave; săn chắc da từ nhau thai cừu</strong></a>, tinh dầu nho v&agrave; vitamin E Lanolin Cream - Careline c&oacute; xuất xừ từ Australia với dung t&iacute;ch 100ml chứa tinh chất dầu hạt nho v&agrave; bổ sung th&ecirc;m vitamine E, cung cấp đầy đủ dưỡng chất chăm s&oacute;c da, nu&ocirc;i dưỡng l&agrave;n da chống lại sự l&atilde;o h&oacute;a, ngăn ngừa chảy xệ, gi&uacute;p l&agrave;n da đ&agrave;n hồi tốt hơn, giảm thiểu c&aacute;c nếp nhăn tr&ecirc;n mặt.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/kem-duong-am-Lanolin-Cream.jpg" style="height:639px; width:960px" /></p>

                <p>&nbsp;</p>',
            ], [
                'name' => 'Son Dưỡng Môi Vaseline Lip Therapy',
                'price' => '40000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620205081/Storage/audvpnwuihpvfjdnv4hz.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Son dưỡng m&ocirc;i Vaseline Lip Therapy gi&uacute;p dưỡng m&ocirc;i n&ecirc;n sử dụng h&agrave;ng ng&agrave;y để gi&uacute;p bảo vệ cho đ&ocirc;i m&ocirc;i tốt nhất.</p>

                <p>- Loại bỏ da kh&ocirc;, chết l&acirc;u ng&agrave;y, cải tạo da cho đ&ocirc;i m&ocirc;i s&aacute;ng hơn , mềm mịn hơn kh&ocirc;ng vết nứt.</p>

                <p>- L&agrave;m son l&oacute;t nền, gi&uacute;p c&aacute;ch li da m&ocirc;i với h&oacute;a chất ch&igrave; của son m&ocirc;i m&agrave;u l&igrave; trang điểm.</p>

                <p>- Đồng thời gi&uacute;p son b&aacute;m m&agrave;u l&acirc;u hơn v&agrave; m&ocirc;i mềm tự nhi&ecirc;n.</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p><a href="http://beautygarden.vn/son-duong-moi-vaseline-lip-therapy.html"><strong>S&aacute;p dưỡng m&ocirc;i Vaseline Rosy Lips Therapy</strong></a>&nbsp;7g c&oacute; xuất xứ từ Mỹ ngay lập tức l&agrave;m mềm m&ocirc;i do được l&agrave;m từ mỡ kho&aacute;ng tinh khiết 100% (100% pure petroleum jelly) tức l&agrave; loại mỡ kho&aacute;ng đ&atilde; được thanh lọc n&ecirc;n rất an to&agrave;n cho mọi loại da, thậm ch&iacute; da m&ocirc;i bạn n&agrave;o bong tr&oacute;c nứt nẻ kinh ni&ecirc;n th&igrave; chỉ c&oacute; vaseline mới c&oacute; khả năng trị được n&oacute; th&ocirc;i nh&eacute;.&nbsp;</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/son-suong-moi-Vaseline-Lip-Therapy.jpg" /></p>',
            ], [
                'name' => 'Son Kem Bourjois Rouge Edition Velvet',
                'price' => '180000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117359/Storage/obtw9b2hzbnz7jcyqcfx.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Son Bourjois dạng kem l&igrave; che khuyết điểm tốt, b&aacute;m m&agrave;u cho đ&ocirc;i m&ocirc;i căng mọng quyến rũ</p>

                <p>- Bảng 19 m&agrave;u sống động, long lanh thoải m&aacute;i cho lựa chọn của mọi m&agrave;u da, t&acirc;m trạng v&agrave; mọi ho&agrave;n cảnh</p>

                <p>- Son Bourjois cung cấp những th&agrave;nh phần dầu dưỡng m&ocirc;i, gi&uacute;p m&ocirc;i kh&ocirc;ng bị th&acirc;m kh&ocirc;, nu&ocirc;i dưỡng m&ocirc;i cho m&agrave;u sắc tươi s&aacute;ng mượt m&agrave;.</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>L&agrave; thương hiệu mỹ phẩm ra đời tại kinh đ&ocirc; thời trang Paris của Ph&aacute;p do nam diễn vi&ecirc;n Joseph Albert Posin s&aacute;ng lập. T&iacute;nh đến nay thương hiệu đ&atilde; c&oacute; lịch sử l&acirc;u đời h&agrave;ng trăm năm, v&agrave; cũng l&agrave; một trong những thương hiệu mỹ phẩm ngoại nổi tiếng tại Việt Nam cũng như thế giới. Mặc d&ugrave; chỉ l&agrave; thương hiệu thuộc ph&acirc;n kh&uacute;c drug store, nhưng c&aacute;c sản phẩm của Bourjois vẫn rất được ưa chuộng bởi vẻ đẹp đơn giản m&agrave; tinh tế, nhẹ nh&agrave;ng nữ t&iacute;nh; c&ocirc;ng dụng tốt sử dụng đơn giản; th&agrave;nh phần th&acirc;n thiện an to&agrave;n đ&atilde; chiếm được cảm t&igrave;nh của kh&ocirc;ng &iacute;t ph&aacute;i đẹp tr&ecirc;n thế giới</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/son-bourjois.jpg" style="height:980px; width:1600px" /></p>

                <p><em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; D&ograve;ng son Bourjois Rouger Edition Velvet đ&igrave;nh đ&aacute;m n&agrave;y được xuất xứ từ nước Ph&aacute;p&nbsp;</em></p>

                <p>Nổi bật trong d&ograve;ng phản phẩm của Bourjois ch&iacute;nh l&agrave; phấn m&aacute; v&agrave; son, đặc biệt sản phẩm&nbsp;<a href="https://beautygarden.vn/son-kem-bourjois-rouge-edition-velvet.html">Son kem Bourjois Rouge Edition Velvet</a><strong>&nbsp;</strong>dạng kem l&igrave; với chất son l&ecirc;n m&ocirc;i mềm mịn, che khuyết điểm gi&uacute;p m&ocirc;i lu&ocirc;n căng mọng lại rất b&aacute;m m&agrave;u, m&agrave;u sắc ch&acirc;n thực sống động đem đến vẻ quyến rũ rạng ngời l&agrave;m say l&ograve;ng ph&aacute;i đẹp. Sản phẩm được blogger tr&ecirc;n khắp thế giới tung h&ocirc; l&agrave; dạng son đỉnh của đỉnh, tạo n&ecirc;n cơn sốt kh&ocirc;ng nhỏ trong thời gian qua.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/Bourjois%20Velvet%20Rouge%20Editiono.jpg" /></p>',
            ], [
                'name' => 'Sữa Rửa Mặt Gokujun Hyaluronic ROHTO',
                'price' => '160000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117391/Storage/y3mvqqsgxufefwfmbpdk.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>☑ Sữa rửa mặt tạo bọt dịu nhẹ</p>

                <p>☑ Giúp bạn lấy sạch bụi bẩn mà không tổn thương da</p>

                <p>☑ Giữ gìn độ ẩm tự nhiên cho da</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>Sữa rửa mặt Hada Labo Gokujyun Face Wash của Rohto Nhật Bản với c&ocirc;ng thức gi&agrave;u dưỡng ẩm v&agrave; độ PH c&acirc;n bằng (5.5) gi&uacute;p l&agrave;m sạch bụi bẩn, b&atilde; nhờn nhưng vẫn bảo tồn độ ẩm tự nhi&ecirc;n cho da. D&ograve;ng sữa rửa mặt tạo bọt n&agrave;y cho da cảm gi&aacute;c mượt m&agrave;, ẩm mịn ngay sau khi sử dụng, kh&ocirc;ng l&agrave;m căng da, kh&ocirc; da.</p>

                <p><strong><a href="http://beautygarden.vn/sua-rua-mat-tao-bot-gokujun-hyaluronic-100g-hadalabo-rohto.html">Sữa rửa mặt tạo bọt Gokujun hyaluronic 100g hadalabo ROHTO</a><br />
                Xuất xứ: Nhật</strong></p>

                <p><strong><img alt="" src="https://adminbeauty.hvnet.vn/Files/Uploads/Sua-rua-mat-tao-bot-Gokujun-hyaluronic-100g-hadalabo-ROHTO.png" /></strong></p>

                <p>&nbsp;</p>

                <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Sữa rửa mặt tạo bọt Gokujun hyaluronic 100g hadalabo ROHTO</p>',
            ], [
                'name' => 'Nước Hoa Hồng Eveline Cosmetics Toning Water',
                'price' => '210000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117529/Storage/pcb17jxka2gusgrqcgax.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Làm dịu các kích ứng, mẩn đỏ</p>

                <p>- Cân bằng độ pH cho da, se lỗ chân lông </p>

                <p>- Làm giảm tiết bã nhờn cho làn da mịn màng sáng khỏe.</p>

                <p>- Dưỡng ẩm sáng da</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN NƯỚC HOA HỒNG EVELINE COSMETICS TONING WATER WITH MAGMA MINERAL MATCHA TEA WATER 400ML</h2>

                <p>Eveline l&agrave; thương hiệu mỹ phẩm nổi tiếng h&agrave;ng đầu Ba Lan v&agrave; đang được tin d&ugrave;ng tại hơn 70 nước tr&ecirc;n to&agrave;n thế giới. Với hơn 30 năm kinh nghiệm trong ng&agrave;nh c&ocirc;ng nghiệp mỹ phẩm, những nh&agrave; s&aacute;ng chế h&agrave;ng đầu Ba Lan đ&atilde; cho ra đời d&ograve;ng mỹ phẩm Eveline c&oacute; chiết xuất ho&agrave;n to&agrave;n từ thi&ecirc;n nhi&ecirc;n, được sản xuất dựa tr&ecirc;n ti&ecirc;u chuẩn v&agrave; c&ocirc;ng nghệ của Dr.Taylor, được Viện Da liễu của Anh khuy&ecirc;n d&ugrave;ng n&ecirc;n hiệu quả v&agrave; an to&agrave;n tuyệt đối với người sử dụng. Những nỗ lực kh&ocirc;ng biết mệt mỏi của Eveline đ&atilde; được ghi nhận bằng h&agrave;ng trăm giải thưởng uy t&iacute;n trong nước v&agrave; quốc tế.</p>

                <p>Nước hoa hồng&nbsp;<strong>Eveline Cosmetics Toning Water With Magma Mineral Matcha Tea Water</strong>&nbsp;ứng dụng c&ocirc;ng nghệ Mirco Derma kh&ocirc;ng chứa Paraben loại bỏ ho&agrave;n to&agrave;n c&aacute;c tạp chất, bụi bẩn, l&agrave;m dịu c&aacute;c k&iacute;ch ứng, mẩn đỏ tr&ecirc;n da, phục hồi độ pH c&acirc;n bằng cho da, se kh&iacute;t lỗ ch&acirc;n l&ocirc;ng, l&agrave;m giảm tiết b&atilde; nhờn cho l&agrave;n da mịn m&agrave;ng, kh&ocirc; tho&aacute;ng. Sản phẩm c&oacute; chứa Hyaluronic Acid, tảo Luminaria cung cấp độ ẩm cho da, l&agrave;m đầy c&aacute;c r&atilde;nh nhăn, cung cấp dưỡng chất cho da ẩm mượt, tươi trẻ.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2019/5/10/a2093844.jpg" /></p>',
            ], [
                'name' => 'Nước Hoa Hồng EvoLuderm Lotion Tonique Sans Alcool',
                'price' => '120000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620632658/Storage/ccsm0igmwgzih0bxpcbx.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Nước hoa hồng giữ ẩm Evoluderm Lotion Tonique cung cấp cho da độ ẩm cần thiết, nuôi dưỡng da mềm mại.</p>

                <p>- Tránh mất nước giúp cho da không bị khô, thiếu sức sống.</p>

                <p>- Có khả năng loại bỏ các tạp chất, bụi bẩn tích tụ dưới lỗ chân lông.</p>

                <p>- Dưỡng ẩm, se khít lỗ chân lông, cân bằng ẩm giúp da luôn mềm mại, sáng hồng.</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p><strong><a href="http://www.beautygarden.vn/nuoc-hoa-hong-giu-am-khong-con-evoluderm-lotion-tonique-sans-alcool.html">Nước hoa hồng giữ ẩm Evoluderm Lotion Tonique</a></strong>&nbsp;l&agrave; sản phẩm c&oacute; xuất xứ từ nước Ph&aacute;p c&oacute; t&aacute;c dụng loại bỏ c&aacute;c tạp chất kh&oacute;i bụi bụi bẩn trong lỗ ch&acirc;n l&ocirc;ng. Sản phẩm n&agrave;y th&iacute;ch hợp với mọi loại da n&ecirc;n c&aacute;c bạn g&aacute;i h&agrave;n to&agrave;n y&ecirc;n t&acirc;m khi sử dụng sản phẩm m&agrave; kh&ocirc;ng lo bị k&iacute;ch ứng da. Dung t&iacute;ch của một chai nước hoa hồng Evoluderm l&agrave; 250ml.</p>

                <p>https://adminbeauty.hvnet.vn/Upload/images/Nuoc-hoa-hong-Evoluderm-Lotion-Tonique-250ml-2.jpg<img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/Nuoc-hoa-hong-Evoluderm-Lotion-Tonique-250ml-2.jpg" style="height:488px; width:650px" /></p>

                <p>&nbsp;</p>',
            ], [
                'name' => 'Sữa Rửa Mặt Cho Da Nhạy Cảm Cetaphil Gentle',
                'price' => '125000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117546/Storage/otnyvcptvejg02zmp4d2.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Nhẹ nhàng, không gây kích ứng</p>

                <p>- Dưỡng ẩm và mềm da</p>

                <p>- Tẩy sạch chất nhờn, bụi bẩn- Không bít lỗ chân lông</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SỮA RỬA MẶT CHO DA NHẠY CẢM CETAPHIL GENTLE SKIN CLEANSER 125ML</h2>

                <p>Rửa mặt l&agrave; bước đầu ti&ecirc;n của qu&aacute; tr&igrave;nh chăm s&oacute;c da. V&igrave; thế việc lựa chọn sản phẩm sữa rửa mặt ph&ugrave; hợp với l&agrave;n da của bạn l&agrave; điều rất quan trọng. Sữa rửa mặt&nbsp;<strong>Cetaphil Daily Facial Cleanser</strong>&nbsp;được c&aacute;c chuy&ecirc;n gia da liễu khuy&ecirc;n d&ugrave;ng.</p>

                <p>L&agrave; dạng gel trong n&ecirc;n rất dễ hấp thụ v&agrave;o da, kh&ocirc;ng m&ugrave;i, kh&ocirc;ng x&agrave; ph&ograve;ng, độ pH trung t&iacute;nh n&ecirc;n mang lại cảm gi&aacute;c dịu nhẹ, kh&ocirc;ng nhờn r&iacute;t hay kh&ocirc; da, tạo cảm gi&aacute;c nhẹ nh&agrave;ng, mềm mại cho da.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2019/5/18/a2022046.jpg" /></p>',
            ], [
                'name' => 'Sữa Rửa Mặt Cho Da Nhạy Cảm Cetaphil Gentle',
                'price' => '125000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117546/Storage/otnyvcptvejg02zmp4d2.png',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Nhẹ nhàng, không gây kích ứng</p>

                <p>- Dưỡng ẩm và mềm da</p>

                <p>- Tẩy sạch chất nhờn, bụi bẩn- Không bít lỗ chân lông</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SỮA RỬA MẶT CHO DA NHẠY CẢM CETAPHIL GENTLE SKIN CLEANSER 125ML</h2>

                <p>Rửa mặt l&agrave; bước đầu ti&ecirc;n của qu&aacute; tr&igrave;nh chăm s&oacute;c da. V&igrave; thế việc lựa chọn sản phẩm sữa rửa mặt ph&ugrave; hợp với l&agrave;n da của bạn l&agrave; điều rất quan trọng. Sữa rửa mặt&nbsp;<strong>Cetaphil Daily Facial Cleanser</strong>&nbsp;được c&aacute;c chuy&ecirc;n gia da liễu khuy&ecirc;n d&ugrave;ng.</p>

                <p>L&agrave; dạng gel trong n&ecirc;n rất dễ hấp thụ v&agrave;o da, kh&ocirc;ng m&ugrave;i, kh&ocirc;ng x&agrave; ph&ograve;ng, độ pH trung t&iacute;nh n&ecirc;n mang lại cảm gi&aacute;c dịu nhẹ, kh&ocirc;ng nhờn r&iacute;t hay kh&ocirc; da, tạo cảm gi&aacute;c nhẹ nh&agrave;ng, mềm mại cho da.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/tinymce/2019/5/18/a2022046.jpg" /></p>',
            ], [
                'name' => 'Kem Nền Dạng Thạch BB Aqua Petit Jelly',
                'price' => '220000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620117496/Storage/e2si3hqtgtqgyhobpn2n.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => '<p>- Chiết xuất từ tinh chất bạc hà và nước giúp dưỡng ẩm và làm mát da</p>

                <p>- Cho lớp trang điểm đẹp hơn, mịn mượt hơn</p>

                <p>- Giữ tone tốt, độ che phủ cao</p>

                <p>- Chất kem mỏng nhẹ, tự nhiên không làm bí da</p>',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM KEM NỀN DẠNG THẠCH BB AQUA PETIT JELLY HOLIKA HOLIKA</h2>

                <p><strong>BB cream&nbsp;dạng thạch&nbsp;Aqua Petit Jelly Holika Holika</strong>&nbsp;40ml từ Hàn Qu&ocirc;́c đã được c&acirc;̣p nh&acirc;̣t tại h&ecirc;̣ th&ocirc;́ng&nbsp;<a href="https://beautygarden.vn/">mỹ ph&acirc;̉m chính hãng</a>&nbsp;Beauty Garden, với chiết xuất từ tinh chất bạc h&agrave; v&agrave; nước, sản ph&acirc;̉m BB Cream của Holika gi&uacute;p dưỡng ẩm, l&agrave;m m&aacute;t da, gi&uacute;p lớp trang điểm đẹp hơn, mịn mượt hơn, giúp giữ tone da, th&ecirc;m vào đó độ che phủ cao nhưng vẫn mỏng v&agrave; tự nhi&ecirc;n cho bạn gái th&ecirc;m ph&acirc;̀n tỏa sáng.</p>

                <p><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/2M8A7290.JPG" /></p>',
            ], [
                'name' => 'Dầu Tẩy Trang TFS Real Blend Deep Cleansing Oil 225ml',
                'price' => '330000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620631175/Storage/sldtfpt62bs7q0i2vhan.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => 'Dầu tẩy trang The Face Shop Real Blend Cleansing Oil chứa thành phần từ dầu jojoba với khả năng giữ ẩm, kháng viêm kháng khuẩn và chứa vitamin E đẩy lùi các dấu hiệu lão hóa, giảm nhăn, mang tới làn da sạch sáng, tươi mới.',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>Tẩy trang hoa c&uacute;c với th&agrave;nh phần kh&ocirc;ng chứa chất tạo m&agrave;u, silicone, dầu kho&aacute;ng, được lấy cảm hứng từ c&ocirc;ng thức chế tạo x&agrave; ph&ograve;ng từ Marseille, Ph&aacute;p.</p>

                <p>★&nbsp;<strong>Real Blend - Cleansing Oil<br />
                Dung t&iacute;ch: 225ml<br />
                Xuất xứ: H&agrave;n Quốc</strong></p>

                <p><strong><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/Real-Blend-Cleansing-Oil.jpg" /></strong></p>',
            ], [
                'name' => 'Sữa Rửa Mặt Rohto Shirochasou White Tea',
                'price' => '150000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620630538/Storage/ilyhrnthwofywbzqbbqt.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => 'Chiết xuất từ từ collagen và nhiều dưỡng chất từ thiên nhiên khác nên nó phù hợp với mọi loại da và không gây kích ứng hay dị ứng gì cho da cả.',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>🍃 Được chiết xuất từ tr&agrave; trắng nguy&ecirc;n chất v&agrave; sữa b&ograve; c&oacute; t&aacute;c dụng l&agrave;m sạch v&agrave; s&aacute;ng da, NGĂN NGỪA MỤN, CHỐNG L&Atilde;O H&Oacute;A, dưỡng ẩm cho da mịn m&agrave;ng, đem lại cảm gi&aacute;c dễ chịu, l&agrave;m săn lỗ ch&acirc;n l&ocirc;ng, d&ugrave;ng được cho cả l&agrave;n da nhạy cảm, da bị mụn, bị sần!</p>

                <p><br />
                <strong>💥 Sữa rửa mặt Tr&agrave; Trắng Nhật<br />
                💦 Khối lượng: 120g<br />
                🏭 Xuất xứ: Nhật Bản</strong></p>

                <p><strong><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/17917564_1875780742689922_3797444233422225039_o.jpg" /></strong></p>

                <p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></p>

                <p>🍃 Chiết xuất từ từ collagen v&agrave; nhiều dưỡng chất từ thi&ecirc;n nhi&ecirc;n kh&aacute;c n&ecirc;n n&oacute; ph&ugrave; hợp với mọi loại da v&agrave; kh&ocirc;ng g&acirc;y k&iacute;ch ứng hay dị ứng g&igrave; cho da cả!</p>',
            ], [
                'name' => 'Bảng Phấn Tạo Khối 3 Màu City Color Contour',
                'price' => '130000',
                'image' => 'http://res.cloudinary.com/snaptec/image/upload/v1620633036/Storage/yojxecfl6tob2s1dius5.jpg',
                'quantity' => '10',
                'manufacturer_id' => '1',
                'description' => 'Bảng phấn tạo khối 3 màu City Color Collection Contour Palette là sản phẩm không chỉ cung cấp màu sắc tuyệt vời, nó còn giúp bạn tiết kiệm một khoản tiền khá lớn. Chất bột phấn mịn màng giúp bạn tạo khối một cách dễ dàng hoàn hảo.',
                'parent_description' => '<h2>TH&Ocirc;NG TIN SẢN PHẨM</h2>

                <p>Bảng phấn tạo khối 3 m&agrave;u cực k&igrave; tiện lợi gi&uacute;p khu&ocirc;n mặt bạn được thon gọn v&agrave; rạng rỡ, mang lại vẻ đẹp tự nhi&ecirc;n v&agrave; thanh tho&aacute;t hơn sau khi make up. V&agrave; nếu kh&eacute;o l&eacute;o hơn, c&aacute;c bạn c&ograve;n c&oacute; thể h&ocirc; biến gi&uacute;p tạo hiệu ứng v&ograve;ng 1 đầy đặn hơn nữa đấy nh&eacute;!</p>

                <p><strong>★&nbsp;<a href="http://beautygarden.vn/danh-muc/trang-diem.html">City Color Collection Contour Palette</a><br />
                Khối lượng: 3x4.5g</strong></p>

                <p><strong><img alt="" src="https://adminbeauty.hvnet.vn/Upload/images/City-Color-Collection-Contour-Palette.jpg" /></strong></p>',
            ]
        ]);
    }
}
