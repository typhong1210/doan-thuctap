<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Manufacturer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manufacturers')->insert([
            [
                'name'=> 'LV'
            ]
        ]);
    }
}
