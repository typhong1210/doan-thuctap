<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Trang Điểm',
                'parent_id' => 0,
            ],
            [
                'name' => 'Chăm Sóc Da',
                'parent_id' => 0,
            ],
            [
                'name' => 'Chăm Sóc Tóc',
                'parent_id' => 0,
            ],
            [
                'name' => 'Nước Hoa',
                'parent_id' => 0,
            ],
            [
                'name' => 'Phụ Kiện',
                'parent_id' => 0,
            ],
            [
                'name' => 'Trang Điểm Mặt',
                'parent_id' => 1,
            ],
            [
                'name' => 'Trang Điểm Mắt',
                'parent_id' => 1,
            ],
            [
                'name' => 'Trang Điểm Môi',
                'parent_id' => 1,
            ],
            [
                'name' => 'Mask-Mặt Nạ',
                'parent_id' => 2,
            ],
            [
                'name' => 'Làm Sạch Da',
                'parent_id' => 2,
            ],
            [
                'name' => 'Dưỡng Da',
                'parent_id' => 2,
            ],
            [
                'name' => 'Set Dưỡng Tóc',
                'parent_id' => 3,
            ],
            [
                'name' => 'Dầu Xả',
                'parent_id' => 3,
            ],
            [
                'name' => 'Dầu Gội Khô',
                'parent_id' => 3,
            ],
            [
                'name' => 'Nước Hoa Nam',
                'parent_id' => 4,
            ],
            [
                'name' => 'Nước Hoa Nữ',
                'parent_id' => 4,
            ],
            [
                'name' => 'Xịt Thơm Body',
                'parent_id' => 4,
            ],
        ]);
    }
}
