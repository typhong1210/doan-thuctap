<?php

use App\Http\Controllers\FrontendController;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//login
Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::name('home.')->group(function(){
    Route::get('/',[FrontEndController::class,'listData']);
    Route::get('/detail/{id}',[FrontendController::class,'productDetail'])->name('detail');
    Route::post('/addToCart',[FrontendController::class,'addToCart'])->name('addtocart');
    Route::get('/cart',[FrontendController::class,'Cart'])->name('cart');
    Route::get('/checkout',[FrontendController::class,'Checkout'])->name('checkout');
    Route::post('order_detail', [FrontendController::class,'orderDetail'])->name('order_detail');
    Route::get('success/{order_id}/{customer_id}', [FrontendController::class,'Success'])->name('success');
    Route::get('products/{id}', [FrontendController::class,'showProduct'])->name('products');
    Route::get('brands/{id}', [FrontendController::class,'showBrand'])->name('brands');
    Route::get('/search',[FrontendController::class,'search'])->name('search');
    Route::post('/searchPost',[FrontendController::class,'searchByName'])->name('searchpost');
    Route::post('/product/tab',[FrontendController::class,'productTab'])->name('tab');
    Route::post('/updateCart',[FrontendController::class,'updateCart'])->name('updateCart');
    Route::post('/removeCart',[FrontendController::class,'removeCart'])->name('removeCart');
    Route::get('send-mail', [FrontendController::class,'sendMail'])->name('sendmail');
    Route::get('about','HomeController@aboutUs')->name('about');
    Route::get('shop','HomeController@Shop')->name('shop');
    Route::get('purchase','FrontendController@purchase')->name('purchase');

});
Route::get('/upload', 'FileUploadController@showUploadForm');
Route::post('/upload', 'FileUploadController@storeUploads');
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::name('admin.')->group(function(){
    Route::get('/view/{id}','HomeController@detailOrder')->name('view');
    Route::get('/order','HomeController@order')->name('order');
    Route::get('/order/detail/{id}','HomeController@orderDetail')->name('orderDetail');
    Route::get('/editOrder/{id}','HomeController@editOrder')->name('editOrder');
    Route::POST('/updateOrder/{id}','HomeController@updateOrder')->name('updateOrder');
});



// URLadmin
Route::get('admin',function (){
    return view('admin.app');
});
Route::prefix('product')->group( function () {
    Route::name('product.')->group( function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::get('/edit/{id}', 'ProductController@edit')->name('edit');
        Route::get('/show/{id}', 'ProductController@show')->name('show');
        Route::put('/update/{id}', 'ProductController@update')->name('update');
        Route::get('/delete/{id}', 'ProductController@destroy')->name('destroy');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::post('/create', 'ProductController@store')->name('store');

    });

Route::prefix('category')->group( function () {
    Route::name('category.')->group( function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
        Route::put('/update/{id}', 'CategoryController@update')->name('update');
        Route::get('/delete/{id}', 'CategoryController@destroy')->name('destroy');
        Route::post('/create', 'CategoryController@store')->name('store');
        Route::get('/create', 'CategoryController@create')->name('create');
    });
});});

Route::prefix('warehouse')->group( function () {
    Route::name('warehouse.')->group( function () {
        Route::get('/', 'WarehouseController@index')->name('index');
        Route::get('/edit/{id}', 'WarehouseController@edit')->name('edit');
        Route::put('/update/{id}', 'WarehouseController@update')->name('update');
        Route::get('/delete/{id}', 'WarehouseController@destroy')->name('destroy');
        Route::post('/create', 'WarehouseController@store')->name('store');
        Route::get('/create', 'WarehouseController@create')->name('create');
        Route::post('/render', 'WarehouseController@renderProduct')->name('render');
    });
});
Route::prefix('category')->group( function () {
    Route::name('category.')->group( function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
        Route::put('/update/{id}', 'CategoryController@update')->name('update');
        Route::get('/delete/{id}', 'CategoryController@destroy')->name('destroy');
        Route::post('/create', 'CategoryController@store')->name('store');
        Route::get('/create', 'CategoryController@create')->name('create');
    });
});
Route::prefix('manufacturers')->group( function () {
    Route::name('manufacturers.')->group( function () {
        Route::get('/', 'ManufacturersController@index')->name('index');
        Route::get('/edit/{id}', 'ManufacturersController@edit')->name('edit');

        Route::put('/update/{id}', 'ManufacturersController@update')->name('update');
        Route::get('/delete/{id}', 'ManufacturersController@destroy')->name('destroy');
        Route::post('/create', 'ManufacturersController@store')->name('store');
        Route::get('/create', 'ManufacturersController@create')->name('create');
    });
});
Route::prefix('customer')->group( function () {
    Route::name('customer.')->group( function () {
        Route::get('/', 'CustomerController@index')->name('index');
        Route::get('/edit/{id}', 'CustomerController@edit')->name('edit');
        Route::get('/show/{id}', 'CustomerController@show')->name('show');
        Route::put('/update/{id}', 'CustomerController@update')->name('update');
        Route::get('/delete/{id}', 'CustomerController@destroy')->name('destroy');
        Route::get('/create', 'CustomerController@create')->name('create');
        Route::post('/create', 'CustomerController@store')->name('store');

    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
